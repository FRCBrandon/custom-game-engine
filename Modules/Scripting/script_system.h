#pragma once
#ifndef SCRIPT_SYSTEM_H
#define SCRIPT_SYSTEM_H

#include "Core/ECS/system.h"

namespace Chin
{
	class ScriptSystem : public SystemBase
	{
	public:
		virtual void ExceuteScript(const std::string& script) = 0;
	};
}

#endif
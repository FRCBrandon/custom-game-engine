#pragma once
#ifndef LUA_SYSTEM_H
#define LUA_SYSTEM_H

#include "script_system.h"
#include "Utility/lua_helper.h"

namespace Chin
{
	class LuaSystem : public ScriptSystem
	{
	public:
		void Initialize() {}
		void Shutdown() {}
		void StepAll(real deltaTime) {}

		void ExceuteScript(const std::string& script) final
		{
			auto result = m_lua.safe_script(script, [](lua_State* L, sol::protected_function_result pfr)
			{
				// pfr will contain things that went wrong, for either loading or executing the script
				// the user can do whatever they like here, including throw. Otherwise...
				sol::error err = pfr;

				Console::luaError(err.what());

				// ... they need to return the protected_function_result
				return pfr;
			});
		}
	private:
		sol::state m_lua;
	};
}

#endif // LUA_SYSTEM_H

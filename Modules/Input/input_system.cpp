#include "input_system.h"
#include "input_component.h"

namespace Chin
{
	InputSystem* InputSystemDependent::m_inputSystem = nullptr;

	InputSystem::InputSystem()
	{
		if(InputSystemDependent::m_inputSystem)
			throw;

		InputSystemDependent::m_inputSystem = this;
	}
}
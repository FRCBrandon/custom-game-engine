#pragma once
#ifndef INPUTCOMPONENT_H
#define INPUTCOMPONENT_H

#include "Core/ECS/component.h"
#include "input_system.h"

namespace Chin
{
	class InputSystemDependent
	{
	public:
		friend class InputSystem;

		static InputSystem* m_inputSystem;
	};

	class InputComponentData : public ComponentBase::Data, public InputSystemDependent
	{
	public:
		InputComponentData(Entity* parent) : Data(parent) {}
	};

	template<typename T>
	class InputComponent : public Component<T>
	{
	public:
		InputComponent(std::string componentName, uint16_t reservedSize) : Component(componentName, ComponentType::Input, reservedSize)
		{
			bool constexpr isInputData = std::is_base_of<InputComponentData, T>::value;
			STATIC_ASSERT(isInputData);
		}
	};
}

#endif
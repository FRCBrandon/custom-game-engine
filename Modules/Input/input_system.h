#pragma once
#ifndef INPUT_SYSTEM_H
#define INPUT_SYSTEM_H

#include "Core/ECS/system.h"

namespace Chin
{
	class InputSystem : public SystemBase
	{
	public:
		InputSystem();

		void PollInput(real deltaTime)
		{
			StepAll(deltaTime);
		}

		virtual void LockMouse(unsigned int x, unsigned int y) = 0;
		virtual void UnlockMouse() = 0;

		virtual void RetrieveMouse() = 0;

		// Replace with vector2
		virtual int GetDeltaMousePositionX() = 0;
		virtual int GetDeltaMousePositionY() = 0;

		virtual void RetrieveKeys() = 0;

		virtual bool CheckKey(unsigned int key) = 0;

		virtual void SetDeltaScroll(float delta) = 0;

		virtual float GetDeltaScroll() = 0;
	};
}

#endif
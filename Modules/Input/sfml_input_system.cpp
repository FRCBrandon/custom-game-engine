#include "sfml_input_system.h"

namespace Chin
{
	void SFMLInputSystem::LockMouse(unsigned int x, unsigned int y)
	{
		m_lockedMouse = true;
		sf::Mouse::setPosition(sf::Vector2i(x, y), m_window);
		m_mousePosition = sf::Vector2i(x, y);
		m_window.setMouseCursorGrabbed(true);
		m_window.setMouseCursorVisible(false);
	}

	void SFMLInputSystem::RetrieveKeys()
	{
		for(char i = sf::Keyboard::A; i != sf::Keyboard::KeyCount; i++)
		{
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Key(i)))
			{
				m_keys[i] = 1;
			}
			else
			{
				m_keys[i] = 0;
			}
		}
	}
}
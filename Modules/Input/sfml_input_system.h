#pragma once
#ifndef SFML_INPUT_SYSTEM_H
#define SFML_INPUT_SYSTEM_H

#include "input_system.h"
#include "Modules/Input/Components/basic_camera_movement_component.h"

#include <SFML/Window.hpp>
#include <bitset>

namespace Chin
{
	// Input System for SFML Window System
	class SFMLInputSystem : public InputSystem
	{
	public:
		SFMLInputSystem(sf::Window& window) :
			m_window(window),
			m_basicCameraMovement(8)
		{}

		void Initialize() final
		{
			RegisterComponent(&m_basicCameraMovement);
		}

		void Shutdown() final
		{
		}

		void ProcessComponents(real deltaTime)
		{
			m_basicCameraMovement.StepAll(deltaTime);
		}

		void StepAll(real deltaTime) final
		{
			if(m_keys.any() || m_deltaScroll != 0.0f || m_deltaMousePosition != sf::Vector2i(0, 0))
			{
				ProcessComponents(deltaTime);

				// reset deltas
				m_deltaScroll = 0.0f;
				m_deltaMousePosition = sf::Vector2i(0, 0);
			}
		}

		void LockMouse(unsigned int x, unsigned int y) final;

		void UnlockMouse() final
		{
			m_lockedMouse = false;
			m_window.setMouseCursorGrabbed(false);
			m_window.setMouseCursorVisible(true);
		}

		void RetrieveMouse() final
		{
			sf::Vector2i currentPosition = sf::Mouse::getPosition(m_window);
			m_deltaMousePosition = sf::Vector2i(currentPosition.x - m_mousePosition.x, currentPosition.y - m_mousePosition.y);
			if(m_lockedMouse)
			{
				sf::Mouse::setPosition(m_mousePosition, m_window);
			}
			else
			{
				m_mousePosition = currentPosition;
			}
		}

		virtual int GetDeltaMousePositionX()
		{
			return m_deltaMousePosition.x;
		}

		virtual int GetDeltaMousePositionY()
		{
			return m_deltaMousePosition.y;
		}

		// Update all keys status
		void RetrieveKeys() final;

		void SetDeltaScroll(float delta) final
		{
			m_deltaScroll = delta;
		}

		float GetDeltaScroll() final
		{
			return m_deltaScroll;
		}

		bool CheckKey(unsigned int key)
		{
			return m_keys[key];
		}

		bool CheckKey(sf::Keyboard::Key key)
		{
			return m_keys[key];
		}
	private:
		std::bitset<128> m_keys; // Holds status of whether the key is pressed or not
		float m_deltaScroll = 0.0f; // Holds delta of scroll wheel for current frame
		bool m_lockedMouse = false; // Holds data whether mouse is locked or not

		sf::Window& m_window; // Reference to window
		sf::Vector2i m_mousePosition; // Last mouse position
		sf::Vector2i m_deltaMousePosition; // Delta from last mouse position to current mouse position

		BasicCameraMovementComponent m_basicCameraMovement;
	};
}

#endif // SFML_INPUT_SYSTEM_H

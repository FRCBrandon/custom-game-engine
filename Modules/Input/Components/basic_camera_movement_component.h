#pragma once
#ifndef BASIC_CAMERA_MOVEMENT_COMPONENT
#define BASIC_CAMERA_MOVEMENT_COMPONENT

#include "Modules/Input/input_component.h"
#include "Core/Data/transform.h"
#include "Modules/Graphics/Abstractions/camera.h"
#include <SFML/Window.hpp>

namespace Chin
{
	struct BasicCameraMovementComponentData : public InputComponentData
	{
	public:
		BasicCameraMovementComponentData(Entity* parent) : InputComponentData(parent)
		{
			OutputBase* cameraOutput = RequireVariable<OutputBase>("Camera", Hash_String("camera"));

			m_camera.SetOutput(cameraOutput);

			OutputBase* transformOutput = RequireVariable<OutputBase>("Transform", Hash_String("transform"));

			m_transform.SetOutput(transformOutput);

			m_camera.Get()->Look(0, 0);
		}

		void Fn(real deltaTime) final
		{
			Camera& camera = *m_camera.Get();

			if(m_inputSystem->CheckKey(sf::Keyboard::W))
			{
				camera.Translate(Camera::Direction::FORWARD, deltaTime);
			}
			if(m_inputSystem->CheckKey(sf::Keyboard::A))
			{
				camera.Translate(Camera::Direction::LEFT, deltaTime);
			}
			if(m_inputSystem->CheckKey(sf::Keyboard::S))
			{
				camera.Translate(Camera::Direction::BACK, deltaTime);
			}
			if(m_inputSystem->CheckKey(sf::Keyboard::D))
			{
				camera.Translate(Camera::Direction::RIGHT, deltaTime);
			}

			if(auto m_deltaScroll = m_inputSystem->GetDeltaScroll())
			{
				camera.Zoom(m_deltaScroll);
			}

			auto deltaX = m_inputSystem->GetDeltaMousePositionX();
			auto deltaY = m_inputSystem->GetDeltaMousePositionY();

			if(deltaX || deltaY)
			{
				camera.Look(deltaX, -deltaY);
			}

			// update position
			m_transform.Get()->SetPosition(camera.GetPosition());

			// update rotation
			m_transform.Get()->SetRotation(camera.GetRotation());
		}

		void OnShift() final
		{
			m_camera.Relink();
			m_transform.Relink();
		}
	private:
		RequiredInput<Camera> m_camera;
		RequiredInput<Transform> m_transform;
	};

	class BasicCameraMovementComponent : public InputComponent<BasicCameraMovementComponentData>
	{
	public:
		BasicCameraMovementComponent(uint16_t reservedSize) : InputComponent("Basic Camera Movement", reservedSize) {}
	};
}

#endif // BASIC_CAMERA_MOVEMENT_COMPONENT

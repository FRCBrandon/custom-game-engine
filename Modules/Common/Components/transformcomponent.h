#pragma once
#ifndef TRANSFORMCOMPONENT_H
#define TRANSFORMCOMPONENT_H

#include "Modules/Common/datacomponent.h"
#include "Core/Data/transform.h"

namespace Chin
{
	struct TransformComponentData : public ComponentBase::Data
	{
	public:
		TransformComponentData(Entity* parent) : Data(parent) {}

		void OnShift()
		{
			m_transform.Relink();
		}

		void* GetVariablePtr(SID variableName) final
		{
			if (variableName == Hash_String("transform"))
				return &m_transform;
			return nullptr;
		}

		std::unique_ptr<DataFieldBase> GetDataField(SID variableName) final
		{
			if(variableName == Hash_String("transform"))
				return CreateDataField<OutputBase>(m_transform);
			return nullptr;
		}

		void Translate(vector3 offSet)
		{
			m_transform->Translate(offSet);
		}

		void Scale(real magnitude)
		{
			m_transform->Scale(magnitude);
		}

		void Rotate(vector3 offSet)
		{
			m_transform->Rotate(offSet);
		}
	private:
		LimitedOutput<Transform, 2> m_transform;
	};

	class TransformComponent : public DataComponent<TransformComponentData>
	{
	public:
		TransformComponent(uint16_t reservedSize) : DataComponent("Transform", reservedSize) {}
	};
}

#endif
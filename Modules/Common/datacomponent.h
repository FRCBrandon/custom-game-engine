#pragma once
#ifndef DATA_COMPONENT_H
#define DATA_COMPONENT_H

#include "Core/ECS/component.h"

namespace Chin
{
	template<typename T>
	class DataComponent : public Component<T>
	{
	public:
		DataComponent(std::string componentName, uint16_t reservedSize) : Component(componentName, ComponentType::Data, reservedSize) {}
	};
}

#endif
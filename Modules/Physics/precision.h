#pragma once
#ifndef PRECisION_H
#define PRECisION_H

#include <cmath>

namespace Chin
{
    typedef float real;

	const real real_pow(const real base, const real exponent);

	const real real_sqrt(const real number);
}

#endif // PRECisION_H

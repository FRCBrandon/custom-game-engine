#pragma once
#ifndef PHYSICSCOMPONENT_H
#define PHYSICSCOMPONENT_H

#include "Core/ECS/component.h"

namespace Chin
{
	template<typename T>
	class PhysicsComponent : public Component<T>
	{
	public:
		PhysicsComponent(std::string componentName, uint16_t reservedSize) : Component(componentName, ComponentType::Physics, reservedSize) {}
	};
}

#endif
#pragma once
#ifndef PHYSICS_SYSTEM_H
#define PHYSICS_SYSTEM_H

#include "Core/ECS/system.h"

namespace Chin
{
	class PhysicsSystem : public SystemBase
	{
	public:
		void Integrate(real deltaTime)
		{
			StepAll(deltaTime);
		}
	};
}

#endif
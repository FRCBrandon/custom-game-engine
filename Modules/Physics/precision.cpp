#include "precision.h"

namespace Chin
{
	const real real_pow(const real base, const real exponent)
	{
		return powf(base, exponent);
	}
	const real real_sqrt(const real number)
	{
		return sqrtf(number);
	}
}
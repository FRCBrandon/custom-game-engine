#pragma once
#ifndef VECTOR3_H
#define VECTOR3_H

#include "Modules/Physics/precision.h"
#include "glm/detail/type_vec3.hpp"
#include <math.h>
#include <GL/glew.h>

namespace Chin
{
	class vector3
	{
	public:
		real m_x = 0.0f, m_y = 0.0f, m_z = 0.0f;
	private:
		real pad;
	public:
		vector3() = default;
		vector3(const real m_x, const real m_y, const real m_z) : m_x(m_x), m_y(m_y), m_z(m_z) {};
		vector3(glm::vec3 vec) : m_x(vec.x), m_y(vec.y), m_z(vec.z) {};

		void Invert();

		real Magnitude() const;

		real SquareMagnitude() const;

		void Normalize();

		void operator*=(const real value);

		vector3 operator*(const real value) const;

		void operator +=(const vector3& v);

		vector3 operator+(const vector3& v) const;

		void operator-=(const vector3& v);

		vector3 operator-(const vector3& v) const;

		void AddScaledVector(const vector3 vector, real scale);

		vector3 ComponentProduct(const vector3 &vector) const;

		void ComponentProductUpdate(const vector3 &vector);

		real ScalarProduct(const vector3 &vector) const;

		real operator*(const vector3 &vector) const;

		vector3 VectorProduct(const vector3 &vector);

		void operator %=(const vector3 &vector);

		vector3 operator%(const vector3 &vector) const;

		vector3 operator=(const vector3 &vector);

		vector3 operator=(const glm::vec3 &vec3);

		void SetX(real value) { m_x = value; }
		real GetX() const { return m_x; }

		void SetY(real value) { m_y = value; }
		real GetY() const { return m_y; }

		void SetZ(real value) { m_z = value; }
		real GetZ() const { return m_z; }
	};

	void MakeOrthonomalBasis(vector3 *a, vector3 *b, vector3 *c);

	glm::vec3 Toglm(const vector3 &vector);
}

#endif // VECTOR3_H

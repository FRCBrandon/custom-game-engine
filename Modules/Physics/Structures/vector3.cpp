#include "vector3.h"

namespace Chin
{
	void vector3::Invert()
	{
	    m_x = -m_x;
	    m_y = -m_y;
	    m_z = -m_z;
	}

	real vector3::Magnitude() const
	{
	    return sqrtf(m_x*m_x+m_y*m_y+m_z*m_z);
	}

	real vector3::SquareMagnitude() const
	{
	    return m_x*m_x+m_y*m_y+m_z*m_z;
	}

	void vector3::Normalize()
	{
	    real l = Magnitude();
	    if(l > 0)
	    {
	        (*this) *= ((real)1)/l;
	    }
	}

	void vector3::operator*=(const real value)
	{
	    m_x *= value;
	    m_y *= value;
	    m_z *= value;
	}

	vector3 vector3::operator*(const real value) const
	{
	    return vector3(m_x*value, m_y*value, m_z*value);
	}

	void vector3::operator +=(const vector3& v)
	{
	    m_x += v.m_x;
	    m_y += v.m_y;
	    m_z += v.m_z;
	}

	vector3 vector3::operator+(const vector3& v) const
	{
	    return vector3(m_x+v.m_x, m_y+v.m_y, m_z+v.m_z);
	}

	void vector3::operator-=(const vector3& v)
	{
	    m_x -= v.m_x;
	    m_y -= v.m_y;
	    m_z -= v.m_z;
	}

	vector3 vector3::operator-(const vector3& v) const
	{
	    return vector3(m_x-v.m_x, m_y-v.m_y, m_z-v.m_z);
	}

	void vector3::AddScaledVector(const vector3 vector, real scale)
	{
	    m_x += vector.m_x * scale;
	    m_y += vector.m_y * scale;
	    m_z += vector.m_z * scale;
	}

	vector3 vector3::ComponentProduct(const vector3 &vector) const
	{
	    return vector3(m_x * vector.m_x, m_y * vector.m_y, m_z * vector.m_z);
	}

	void vector3::ComponentProductUpdate(const vector3 &vector)
	{
	    m_x *= vector.m_x;
	    m_y *= vector.m_y;
	    m_z *= vector.m_z;
	}

	real vector3::ScalarProduct(const vector3 &vector) const
	{
	    return m_x*vector.m_x + m_y*vector.m_y + m_z*vector.m_z;
	}

	real vector3::operator*(const vector3 &vector) const
	{
	    return m_x*vector.m_x + m_y*vector.m_y + m_z*vector.m_z;
	}

	vector3 vector3::VectorProduct(const vector3 &vector)
	{
	    return vector3(m_y*vector.m_z-m_z*vector.m_y, m_z*vector.m_x-m_x*vector.m_z, m_x*vector.m_y-m_y*vector.m_x);
	}

	void vector3::operator%=(const vector3 &vector)
	{
	    *this = VectorProduct(vector);
	}

	vector3 vector3::operator%(const vector3 &vector) const
	{
	    return vector3(m_y*vector.m_z-vector.m_y, m_z*vector.m_x-m_x*vector.m_z, m_x*vector.m_y-m_y*vector.m_x);
	}

	vector3 vector3::operator=(const vector3 &vector)
	{
	    this->m_x = vector.m_x;
	    this->m_y = vector.m_y;
	    this->m_z = vector.m_z;

		return *this;
	}

	vector3 vector3::operator=(const glm::vec3 &vec3)
	{
	    return vector3(vec3.x, vec3.y, vec3.z);
	}

	void Chin::MakeOrthonomalBasis(vector3 *a, vector3 *b, vector3 *c)
	{
	    a->Normalize();
	    (*c) = (*a) % (*b);
	    if( c->SquareMagnitude() == 0.0) return;
	    c->Normalize();
	    (*b) = (*c) % (*a);
	}

	glm::vec3 Chin::Toglm (const vector3 &vector)
	{
	    return glm::vec3(vector.m_x, vector.m_y, vector.m_z);
	}
}
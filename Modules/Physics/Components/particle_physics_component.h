#pragma once
#ifndef PARTICLE_PHYSICS_COMPONENT_H
#define PARTICLE_PHYSICS_COMPONENT_H

#include "Modules/Physics/physics_component.h"
#include "Core/Misc/input.h"
#include "Core/Data/transform.h"

namespace Chin
{
	struct ParticlePhysicsComponentData : public ComponentBase::Data
	{
	public:
		ParticlePhysicsComponentData(Entity* parent) : Data(parent)
		{
			OutputBase* positionOutput = RequireVariable<OutputBase>("Transform", Hash_String("transform"));

			m_position.SetOutput(positionOutput);
		}

		~ParticlePhysicsComponentData() = default;

		void Fn(real deltaTime) final
		{
			if (m_inverseMass <= 0.0f)
				return;

			m_position.Get()->Translate(m_velocity * deltaTime);

			m_velocity.AddScaledVector(m_acceleration, deltaTime);

			m_velocity *= real_pow(m_damping, deltaTime);
		}

		void OnShift()
		{
			m_position.Relink();
		}

		void* GetVariablePtr(SID variableName)
		{
			if (variableName == Hash_String("position"))
				return &m_position;
			else if (variableName == Hash_String("velocity"))
				return &m_velocity;
			else if (variableName == Hash_String("acceleration"))
				return &m_acceleration;
			else if (variableName == Hash_String("inverseMass"))
				return &m_inverseMass;
			else if (variableName == Hash_String("damping"))
				return &m_damping;

			return nullptr;
		}

		std::unique_ptr<DataFieldBase> GetDataField(SID variableName) final
		{
			if (variableName == Hash_String("position"))
				return CreateDataField<InputBase>(m_position);
			else if (variableName == Hash_String("velocity"))
				return CreateDataField(m_velocity);
			else if (variableName == Hash_String("acceleration"))
				return CreateDataField(m_acceleration);
			else if (variableName == Hash_String("inverseMass"))
				return CreateDataField(m_inverseMass);
			else if (variableName == Hash_String("damping"))
				return CreateDataField(m_damping);

			return nullptr;
		}

		void SetMass(const real mass)
		{
			ASSERT(mass != 0);
			m_inverseMass = ((real)1.0) / mass;
		}

		void SetInverseMass(const real mass)
		{
			m_inverseMass = mass;
		}
	private:
		RequiredInput<Transform> m_position;
		vector3 m_velocity, m_acceleration;
		real m_inverseMass = 0, m_damping = 1.0;
	};

	class ParticlePhysicsComponent : public PhysicsComponent<ParticlePhysicsComponentData>
	{
	public:
		ParticlePhysicsComponent() : PhysicsComponent("Particle Physics", g_defaultReservedComponentSize) {}
	};
}

#endif
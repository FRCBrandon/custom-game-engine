#pragma once
#ifndef CHIN_PHYSICS_SYSTEM_H
#define CHIN_PHYSICS_SYSTEM_H

#include "physics_system.h"
#include "Components/particle_physics_component.h"

namespace Chin
{
	class ChinPhysicsSystem : public PhysicsSystem
	{
	public:
		ChinPhysicsSystem() :
			m_particlePhysics()
		{}

		void Initialize() final
		{
			RegisterComponent(&m_particlePhysics);
		}

		void Shutdown() final
		{
		}

		void StepAll(real deltaTime) final
		{
			m_particlePhysics.StepAll(deltaTime);
		}
	private:
		ParticlePhysicsComponent m_particlePhysics;
	};
}

#endif
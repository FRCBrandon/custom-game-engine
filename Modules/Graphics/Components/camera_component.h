#pragma once
#ifndef CAMERA_COMPONENT_H
#define CAMERA_COMPONENT_H

#include "Modules/Graphics/graphics_component.h"
#include "Core/Misc/output.h"

#include "Modules/Graphics/Abstractions/camera.h"

namespace Chin
{
	struct CameraComponentData : public GraphicsComponentData
	{
	public:
		CameraComponentData(Entity* parent) : GraphicsComponentData(parent)
		{
			m_graphicsSystem->SetCamera(&m_camera.GetRef());
			//m_camera.GetRef().
		}

		void Fn(real deltaTime) final
		{

		}

		void OnShift() final
		{
			m_camera.Relink();
		}

		void* GetVariablePtr(SID variableName) final
		{
			if(variableName == Hash_String("camera"))
				return &m_camera;
			return nullptr;
		}

		std::unique_ptr<DataFieldBase> GetDataField(SID variableName) final
		{
			if(variableName == Hash_String("camera"))
				return CreateDataField<OutputBase>(m_camera);
			return nullptr;
		}
	private:
		LimitedOutput<Camera, 1> m_camera;
	};

	class CameraComponent : public GraphicsComponent<CameraComponentData>
	{
	public:
		CameraComponent() : GraphicsComponent("Camera", 4) {}
	};
}

#endif
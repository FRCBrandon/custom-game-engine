#pragma once
#ifndef MESH_COMPONENT_H
#define MESH_COMPONENT_H

#include "Modules/Common/datacomponent.h"
#include "Modules/Graphics/Mesh/mesh.h"

namespace Chin
{
	struct MeshComponentData : public ComponentBase::Data
	{
	public:
		MeshComponentData(Entity* parent) : Data(parent), m_mesh("Resources/Meshes/box.obj"
			//, "Resources/Textures/container.png"
		)
		{}

		void OnShift() final
		{
			m_mesh.Relink();
		}

		void* GetVariablePtr(SID variableName) final
		{
			if(variableName == Hash_String("mesh"))
				return &m_mesh;
			return nullptr;
		}

		std::unique_ptr<DataFieldBase> GetDataField(SID variableName) final
		{
			if(variableName == Hash_String("mesh"))
				return CreateDataField<OutputBase>(m_mesh);
			return nullptr;
		}
	private:
		LimitedOutput<Mesh, 1> m_mesh;
	};

	class MeshComponent : public DataComponent<MeshComponentData>
	{
	public:
		MeshComponent() : DataComponent("Mesh", g_defaultReservedComponentSize)
		{}
	};
}

#endif
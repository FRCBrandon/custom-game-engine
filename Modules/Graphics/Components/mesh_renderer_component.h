#pragma once
#ifndef MESH_RENDERER_COMPONENT_H
#define MESH_RENDERER_COMPONENT_H

#include "Modules/Graphics/graphics_component.h"
#include "Core/Data/transform.h"

namespace Chin
{
	struct MeshRendererComponentData : public GraphicsComponentData
	{
	public:
		MeshRendererComponentData(Entity* parent) : GraphicsComponentData(parent)
		{
			auto o_transform = RequireVariable<OutputBase>("Transform", Hash_String("transform"));

			m_transform.SetOutput(o_transform);

			auto o_mesh = RequireVariable<OutputBase>("Mesh", Hash_String("mesh"));

			m_mesh.SetOutput(o_mesh);
		}

		void Fn(real deltaTime) final
		{
			glm::mat4 model;

			Transform& transform = *m_transform.Get();

			model = glm::translate(model, Toglm(transform.GetPosition()));
			model = glm::scale(model, Toglm(transform.GetScale()));

			m_graphicsSystem->GetStandardProgramme().SetMat4("model", model);

			m_graphicsSystem->RenderDrawable(*m_mesh.Get());
		}

		void OnShift() final
		{
			m_transform.Relink();
			m_mesh.Relink();
		}
	private:
		RequiredInput<Transform> m_transform;
		RequiredInput<Mesh> m_mesh;
	};

	class MeshRendererComponent : public GraphicsComponent<MeshRendererComponentData>
	{
	public:
		MeshRendererComponent() : GraphicsComponent("Mesh Renderer", g_defaultReservedComponentSize)
		{}
	};
};

#endif
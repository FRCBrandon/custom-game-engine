#pragma once
#ifndef LIGHT_COMPONENT_H
#define LIGHT_COMPONENT_H

#include "Modules/Graphics/graphics_component.h"
#include "Core/Data/transform.h"
#include "Modules/Graphics/Abstractions/light.h"

namespace Chin
{
	struct LightComponentData : public GraphicsComponentData
	{
	public:
		LightComponentData(Entity* parent) : GraphicsComponentData(parent)
		{
			OutputBase* transformOutput = RequireVariable<OutputBase>("Transform", Hash_String("transform"));

			m_transform.SetOutput(transformOutput);

			SetLightType(Light::Type::Directional);
		}

		void Fn(real deltaTime) final
		{
			Transform& transform = *m_transform.Get();

			if(m_position)
			{
				*m_position = Toglm(transform.GetPosition());
			}
			if(m_rotation)
			{
				*m_rotation = Toglm(transform.GetRotation());
			}
		}

		void OnShift() final
		{
			m_transform.Relink();
		}

		void SetLightType(Light::Type type)
		{
			switch(type)
			{
			case Light::Type::Directional:
				m_light = DirectionalLight();
				m_position = nullptr;
				m_rotation = &std::get<DirectionalLight>(m_light).m_direction;
				break;
			case Light::Type::Point:
				m_light = PointLight();
				m_position = &std::get<PointLight>(m_light).m_position;
				m_rotation = nullptr;
				break;
			case Light::Type::Spot:
				m_light = SpotLight();
				m_position = &std::get<SpotLight>(m_light).m_position;
				m_rotation = &std::get<SpotLight>(m_light).m_direction;
				break;
			default:
				ASSERT(false);
				break;
			}

			// Pointer in graphics system equals nullptr
			if(m_lightPtr)
				*m_lightPtr = nullptr;

			// Point to pointer in graphics system
			m_lightPtr = m_graphicsSystem->SetLight(m_light);
		}
	private:
		RequiredInput<Transform> m_transform;
		GenericLight m_light;
		Light** m_lightPtr = nullptr;
		glm::vec3 *m_position = nullptr, *m_rotation = nullptr;
	};

	class LightComponent : public GraphicsComponent<LightComponentData>
	{
	public:
		LightComponent() : GraphicsComponent("Light", 128) {}
	};
}

#endif
#pragma once
#ifndef GRAPHICSSYSTEM_H
#define GRAPHICSSYSTEM_H

#include "Core/ECS/system.h"
#include "Modules/Graphics/Mesh/drawable.h"
#include "Modules/Graphics/Abstractions/camera.h"
#include "Modules/Graphics/Abstractions/light.h"
#include "Modules/Graphics/Shader/shaderprogramme.h"

namespace Chin
{
	class GraphicsSystem : public SystemBase
	{
	public:
		GraphicsSystem();

		void RenderAll(real deltaTime)
		{
			StepAll(deltaTime);
		}

		void RenderDrawable(Drawable& drawable)
		{
			drawable.Render();
		}

		virtual void Resize(const unsigned int width, const unsigned int height) = 0;

		virtual void SetCamera(Camera* camera) = 0;

		virtual Light** SetLight(GenericLight& light) = 0;

		virtual ShaderProgramme& GetStandardProgramme() = 0;
	};
}

#endif
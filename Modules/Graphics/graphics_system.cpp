#include "graphics_system.h"
#include "graphics_component.h"

namespace Chin
{
	GraphicsSystem* GraphicsSystemDependent::m_graphicsSystem = nullptr;

	GraphicsSystem::GraphicsSystem()
	{
		if(GraphicsSystemDependent::m_graphicsSystem)
			throw;

		GraphicsSystemDependent::m_graphicsSystem = this;
	}
}
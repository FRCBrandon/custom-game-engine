#pragma once
#ifndef GRAPHICS_COMPONENT_H
#define GRAPHICS_COMPONENT_H

#include "Core/ECS/component.h"
#include "graphics_system.h"

namespace Chin
{
	class GraphicsSystemDependent
	{
	public:
		friend class GraphicsSystem;

		static GraphicsSystem* m_graphicsSystem;
	};

	class GraphicsComponentData : public ComponentBase::Data, public GraphicsSystemDependent
	{
	public:
		GraphicsComponentData(Entity* parent) : Data(parent) {}
	};

	template<typename T>
	class GraphicsComponent : public Component<T>
	{
	public:
		GraphicsComponent(std::string componentName, uint16_t reservedSize) : Component(componentName, ComponentType::Graphics, reservedSize)
		{
			bool constexpr isGraphicsData = std::is_base_of<GraphicsComponentData, T>::value;
			STATIC_ASSERT(isGraphicsData);
		}
	};
}

#endif
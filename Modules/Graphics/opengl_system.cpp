#include "opengl_system.h"

namespace Chin
{
	void OpenGLSystem::Render()
	{
		// Clear buffers
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// ReSet model matrix
		m_model = glm::mat4();

		// Calculate view and projection matrices based on currentCamera
		// Set view position in standard programme
		if(m_currentCamera)
		{
			m_view = LookAt(*m_currentCamera);
			m_projection = glm::perspective(m_currentCamera->GetFOVRadians(), m_width / m_height, 0.1f, 100.0f);
			m_standardProgramme.SetVec3("viewPos", m_currentCamera->GetPosition());
		}


		// Set matrices in standard programme
		m_standardProgramme.SetMat4("model", m_model);
		m_standardProgramme.SetMat4("view", m_view);
		m_standardProgramme.SetMat4("proj", m_projection);

		m_standardProgramme.Use();

		// Update lights
		UpdateLights();

		// Render components
		StepAll(0.0);

		// Render meshes and flyweights for each shader programme
		/*
		for(DrawablesList shaderProgramme : m_shaderProgrammes)
		{

		auto programme = shaderProgramme.first;
		GLuint id = programme.GetProgrammeID();

		programme.Use();

		auto& drawables = shaderProgramme.second;
		for(Drawable& drawable : drawables)
		{
		drawable.Render();
		}

		}*/
	}
}
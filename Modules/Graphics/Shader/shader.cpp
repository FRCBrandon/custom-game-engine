#include "shader.h"

namespace Chin
{
	Shader::~Shader()
	{
	    glDeleteShader(m_shaderID);
	}

	GLuint Shader::GetShaderID()
	{
	    return m_shaderID;
	}
}

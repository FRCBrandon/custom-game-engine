#pragma once
#ifndef SHADER_H
#define SHADER_H

#include "Core/Parsers/loader.h"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>

namespace Chin
{
	// Loads Shader from file
	// Returns Shader's ID
	//GLuint loadShader(const std::string& filepath);

	static void _print_shader_info_log(GLuint shader_index)
	{
		int max_length = 2048;
		int actual_length = 0;
		char shader_log[2048];
		glGetShaderInfoLog(shader_index, max_length, &actual_length, shader_log);
		printf("shader info log for GL index %u:\n%s\n", shader_index, shader_log);
	}

	// Shader for ShaderProgrammes
	class Shader
	{
	public:
		// Constructs empty Shader
		// For ShaderProgramme use only
		Shader() {}

		// Constructs Shader from file
	    Shader(const std::string& filePath)
		{
			Create(RetrieveShaderType(filePath));
			LoadShader(filePath);
			Log();
		}

		// Destructs Shader
	    ~Shader();

		void Create(GLenum shaderType)
		{
			SetShaderID(glCreateShader(shaderType));
		}

		static GLenum RetrieveShaderType(const std::string& filePath)
		{
			if(filePath.rfind(".vert") != std::string::npos)
				return GL_VERTEX_SHADER;
			else if(filePath.rfind(".frag") != std::string::npos)
				return GL_FRAGMENT_SHADER;
			else if(filePath.rfind(".geom") != std::string::npos)
				return GL_GEOMETRY_SHADER;
		}

		void LoadShader(const std::string& filePath)
		{
			ASSERT(m_shaderID);

			std::string shaderData = readFile(filePath);
			const char* shader = shaderData.c_str();

			glShaderSource(m_shaderID, 1, &shader, nullptr);
			glCompileShader(m_shaderID);
		}

		void Log()
		{
			ASSERT(m_shaderID);

			int params = -1;
			glGetShaderiv(m_shaderID, GL_COMPILE_STATUS, &params);
			if(GL_TRUE != params)
			{
				Console::Error("GL shader index "+ std::to_string(m_shaderID) +" did not compile");
				_print_shader_info_log(m_shaderID);
				return;
			}
			Console::Log("Shader " + std::to_string(m_shaderID) + " loaded");
			Console::Log("Maximum Vertex Uniforms: " + std::to_string(GL_MAX_VERTEX_UNIFORM_COMPONENTS_ARB) + " | Maximum Fragment Uniforms: " + std::to_string(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS_ARB));
		}

		// Empty
	    //GLint addVariable(const char* variableName);

		// Get Shader's ID
	    GLuint GetShaderID();
	private:
		GLuint m_shaderID = 0; // Shader's ID

		//std::vector<GLuint> m_vars;

		void SetShaderID(GLuint id)
		{
			m_shaderID = id;
		}
	};
}

#endif // SHADER_H

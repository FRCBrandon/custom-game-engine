#pragma once
#ifndef SHADERPROGRAMME_H
#define SHADERPROGRAMME_H

#include "shader.h"
#include "Modules/Graphics/Abstractions/light.h"
#include <array>

namespace Chin
{
	// Used to switch shaders in RenderSystem
	class ShaderProgramme
	{
	public:
		// Constructs empty ShaderProgramme
		// Only creates ID
	    ShaderProgramme();

		// Constructs ShaderProramme with .vert file, .frag file, .geom files
		// Doesn't use class Shaders
	    ShaderProgramme(const std::string& shaderVertex, const std::string& shaderFragment, const std::string& shaderGeometry = "");

		// Construct ShaderProgramme from ShaderProgramme file
		// Uses class Shaders
		ShaderProgramme(const std::string& filePath);

		//ShaderProgramme(ShaderProgramme&& source) {}
		//ShaderProgramme &operator=(const ShaderProgramme&& source) { return *this; }

		// Destructs ShaderProgramme and its Shaders
	    ~ShaderProgramme();

		void Create()
		{
			m_programmeID = glCreateProgram();
		}

		// Add Shader with linking
	    void AddShader(const std::string& filePath);

		// Attach Shader without linking
		void AttachShader(const std::string& filePath);

		// Links ShaderProgramme
		void Link();

		// Load programme (.programme) files
		void Load(const std::string& filePath);

		// Use ShaderProgramme
	    void Use();

	    void SetBool(const std::string& name, bool value) const;
	    void SetInt(const std::string& name, int value) const;
	    void SetFloat(const std::string& name, float value) const;
	    void SetVec2(const std::string& name, const glm::vec2& value) const;
	    void SetVec2(const std::string& name, float x, float y) const;
	    void SetVec3(const std::string& name, const glm::vec3& value) const;
	    void SetVec3(const std::string&name, float x, float y, float z) const;
	    void SetVec4(const std::string& name, const glm::vec4& value) const;
	    void SetVec4(const std::string& name, float x, float y, float z, float w) const;
	    void SetMat2(const std::string& name, const glm::mat2& mat) const;
	    void SetMat3(const std::string& name, const glm::mat3& mat) const;
	    void SetMat4(const std::string& name, const glm::mat4& mat) const;
		void SetDirectionalLight(const std::string& name, const DirectionalLight& light);
		void SetPointLight(const std::string& name, const PointLight& light);
		void SetSpotLight(const std::string& name, const SpotLight& light);

		// Get Programme ID
	    GLuint GetProgrammeID();
	private:
		GLuint m_programmeID = 0; // ID of ShaderProgramme

		std::array<Shader, 3> m_shaders = {Shader()}; // Array of Shaders
	};
}

#endif // SHADERPROGRAMME_H

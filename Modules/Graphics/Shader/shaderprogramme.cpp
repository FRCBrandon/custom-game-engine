#include "shaderprogramme.h"

namespace Chin
{
	void _print_programme_info_log(GLuint programme)
	{
	    int max_length = 2048;
	    int actual_length = 0;
	    char program_log[2048];
	    glGetProgramInfoLog(programme, max_length, &actual_length, program_log);
	    printf("program info log for GL index %u:\n%s", programme, program_log);
	}

	const char* GL_type_to_string(GLenum type)
	{
	    switch(type)
	    {
	    case GL_BOOL:
	        return "bool";
	    case GL_INT:
	        return "int";
	    case GL_FLOAT:
	        return "float";
	    case GL_FLOAT_VEC2:
	        return "vec2";
	    case GL_FLOAT_VEC3:
	        return "vec3";
	    case GL_FLOAT_VEC4:
	        return "vec4";
	    case GL_FLOAT_MAT2:
	        return "mat2";
	    case GL_FLOAT_MAT3:
	        return "mat3";
	    case GL_FLOAT_MAT4:
	        return "mat4";
	    case GL_SAMPLER_2D:
	        return "sampler2D";
	    case GL_SAMPLER_3D:
	        return "sampler3D";
	    case GL_SAMPLER_CUBE:
	        return "samplerCube";
	    case GL_SAMPLER_2D_SHADOW:
	        return "sampler2DShadow";
	    default:
	        break;
	    }
	    return "other";
	}

	void print_all(GLuint programme)
	{
	    printf("--------------------\nshader programme %i info:\n", programme);
	    int params = -1;
	    glGetProgramiv(programme, GL_LINK_STATUS, &params);
	    printf("GL_LINK_STATUS = %i\n", params);

	    glGetProgramiv(programme, GL_ATTACHED_SHADERS, &params);
	    printf("GL_ATTACHED_SHADERS = %i\n", params);

	    glGetProgramiv(programme, GL_ACTIVE_ATTRIBUTES, &params);
	    printf("GL_ACTIVE_ATTRIBUTES = %i\n", params);
	    for (int i = 0; i < params; i++)
	    {
	        char name[64];
	        int max_length = 64;
	        int actual_length = 0;
	        int size = 0;
	        GLenum type;
	        glGetActiveAttrib (
	            programme,
	            i,
	            max_length,
	            &actual_length,
	            &size,
	            &type,
	            name
	        );
	        if (size > 1)
	        {
	            for(int j = 0; j < size; j++)
	            {
	                char long_name[64];
	                sprintf_s(long_name, "%s[%i]", name, j);
	                int location = glGetAttribLocation(programme, long_name);
	                printf("  %i) type:%s name:%s location:%i\n",
	                       i, GL_type_to_string(type), long_name, location);
	            }
	        }
	        else
	        {
	            int location = glGetAttribLocation(programme, name);
	            printf("  %i) type:%s name:%s location:%i\n",
	                   i, GL_type_to_string(type), name, location);
	        }
	    }

	    glGetProgramiv(programme, GL_ACTIVE_UNIFORMS, &params);
	    printf("GL_ACTIVE_UNIFORMS = %i\n", params);
	    for(int i = 0; i < params; i++)
	    {
	        char name[64];
	        int max_length = 64;
	        int actual_length = 0;
	        int size = 0;
	        GLenum type;
	        glGetActiveUniform(
	            programme,
	            i,
	            max_length,
	            &actual_length,
	            &size,
	            &type,
	            name
	        );
	        if(size > 1)
	        {
	            for(int j = 0; j < size; j++)
	            {
	                char long_name[64];
					sprintf_s(long_name, "%s[%i]", name, j);
	                int location = glGetUniformLocation(programme, long_name);
	                printf("  %i) type:%s name:%s location:%i\n",
	                       i, GL_type_to_string(type), long_name, location);
	            }
	        }
	        else
	        {
	            int location = glGetUniformLocation(programme, name);
	            printf("  %i) type:%s name:%s location:%i\n",
	                   i, GL_type_to_string(type), name, location);
	        }
	    }

	    _print_programme_info_log(programme);
	}

	ShaderProgramme::ShaderProgramme()
	{
	}

	ShaderProgramme::ShaderProgramme(const std::string& shaderVertex, const std::string& shaderFragment, const std::string& shaderGeometry)
	{
	    m_programmeID = glCreateProgram();

	    AttachShader(shaderVertex);
	    AttachShader(shaderFragment);
	    AttachShader(shaderGeometry);

		Link();
	}

	ShaderProgramme::ShaderProgramme(const std::string& filePath)
	{
	    m_programmeID = glCreateProgram();
		Load(filePath);
	}

	ShaderProgramme::~ShaderProgramme()
	{
	    for(auto it : m_shaders)
	    {
	        glDetachShader(m_programmeID, it.GetShaderID());
	    }
	    glUseProgram(0);
	    glDeleteProgram(m_programmeID);
	}

	void ShaderProgramme::AddShader(const std::string& filePath)
	{
		try
		{
			AttachShader(filePath);
		}
		catch(...)
		{
		}

		Link();
	}

	void ShaderProgramme::AttachShader(const std::string& filePath)
	{
		if (filePath.empty())
			return;

		unsigned char position = 0;
		auto shaderType = Shader::RetrieveShaderType(filePath);
		switch(shaderType)
		{
		case GL_VERTEX_SHADER:
			position = 0;
			break;
		case GL_FRAGMENT_SHADER:
			position = 1;
			break;
		case GL_GEOMETRY_SHADER:
			position = 2;
			break;
		default:
			Console::Error("Error in ShaderProgramme");
			return;
		}

		Shader& shader = m_shaders[position];

		if(shader.GetShaderID() == 0)
		{
			shader.Create(shaderType);
			shader.LoadShader(filePath);
			shader.Log();

			glAttachShader(m_programmeID, shader.GetShaderID());
		}
		else
		{
			shader.LoadShader(filePath);
			shader.Log();
		}
	}

	void ShaderProgramme::Link()
	{
		glLinkProgram(m_programmeID);

		int params = -1;
		glGetProgramiv(m_programmeID, GL_LINK_STATUS, &params);
		if (GL_TRUE != params)
		{
			Console::Error( "Could not link shader m_programmeID: " + std::to_string(m_programmeID) + " GL index" );
			_print_programme_info_log(m_programmeID);
		}
		print_all(m_programmeID);
	}

	void ShaderProgramme::Load(const std::string& filePath)
	{
		std::ifstream file(filePath);

		std::string line = "";
		if(file.is_open())
			while(std::getline(file, line))
				AttachShader(line.c_str());

		file.close();

		Link();
	}

	void ShaderProgramme::Use()
	{
	    glUseProgram(m_programmeID);
	}

	void ShaderProgramme::SetBool(const std::string& name, bool value) const
	{
	    glUniform1i(glGetUniformLocation(m_programmeID, name.c_str()), (int)value);
	}

	void ShaderProgramme::SetInt(const std::string& name, int value) const
	{
	    glUniform1i(glGetUniformLocation(m_programmeID, name.c_str()), value);
	}

	void ShaderProgramme::SetFloat(const std::string& name, float value) const
	{
	    glUniform1f(glGetUniformLocation(m_programmeID, name.c_str()), value);
	}

	void ShaderProgramme::SetVec2(const std::string& name, const glm::vec2& value) const
	{
	    glUniform2fv(glGetUniformLocation(m_programmeID, name.c_str()), 1, glm::value_ptr(value));
	}

	void ShaderProgramme::SetVec2(const std::string& name, float x, float y) const
	{
	    glUniform2f(glGetUniformLocation(m_programmeID, name.c_str()), x, y);
	}

	void ShaderProgramme::SetVec3(const std::string& name, const glm::vec3& value) const
	{
	    glUniform3fv(glGetUniformLocation(m_programmeID, name.c_str()), 1, glm::value_ptr(value));
	}

	void ShaderProgramme::SetVec3(const std::string& name, float x, float y, float z) const
	{
	    glUniform3f(glGetUniformLocation(m_programmeID, name.c_str()), x, y, z);
	}

	void ShaderProgramme::SetVec4(const std::string& name, const glm::vec4& value) const
	{
	    glUniform4fv(glGetUniformLocation(m_programmeID, name.c_str()), 1, glm::value_ptr(value));
	}

	void ShaderProgramme::SetVec4(const std::string& name, float x, float y, float z, float w) const
	{
	    glUniform4f(glGetUniformLocation(m_programmeID, name.c_str()), x, y, z, w);
	}

	void ShaderProgramme::SetMat2(const std::string& name, const glm::mat2& mat) const
	{
	    glUniformMatrix2fv(glGetUniformLocation(m_programmeID, name.c_str()), 1, GL_FALSE, glm::value_ptr(mat));
	}

	void ShaderProgramme::SetMat3(const std::string& name, const glm::mat3& mat) const
	{
	    glUniformMatrix3fv(glGetUniformLocation(m_programmeID, name.c_str()), 1, GL_FALSE, glm::value_ptr(mat));
	}

	void ShaderProgramme::SetMat4(const std::string& name, const glm::mat4& mat) const
	{
	    glUniformMatrix4fv(glGetUniformLocation(m_programmeID, name.c_str()), 1, GL_FALSE, glm::value_ptr(mat));
	}

	void ShaderProgramme::SetDirectionalLight(const std::string& name, const DirectionalLight& light)
	{
		SetVec3(name + ".direction", light.m_direction);
		SetVec3(name + ".ambient", light.m_ambient);
		SetVec3(name + ".diffuse", light.m_diffuse);
		SetVec3(name + ".specular", light.m_specular);
	}

	void ShaderProgramme::SetPointLight(const std::string& name, const PointLight& light)
	{
		SetVec3(name + ".position", light.m_position);
		SetFloat(name + ".constant", light.m_constant);
		SetFloat(name + ".linear", light.m_linear);
		SetFloat(name + ".quadratic", light.m_quadratic);
		SetVec3(name + ".ambient", light.m_ambient);
		SetVec3(name + ".diffuse", light.m_diffuse);
		SetVec3(name + ".specular", light.m_specular);
	}

	void ShaderProgramme::SetSpotLight(const std::string& name, const SpotLight& light)
	{
		SetVec3(name + ".position", light.m_position);
		SetVec3(name + ".direction", light.m_direction);
		SetVec3(name + ".ambient", light.m_ambient);
		SetVec3(name + ".diffuse", light.m_diffuse);
		SetVec3(name + ".specular", light.m_specular);
		SetFloat(name + ".constant", light.m_constant);
		SetFloat(name + ".linear", light.m_linear);
		SetFloat(name + ".quadratic", light.m_quadratic);
		SetFloat(name + ".cutoff", light.m_cutoff);
		SetFloat(name + ".outerCutoff", light.m_outerCutoff);
	}

	GLuint ShaderProgramme::GetProgrammeID()
	{
	    return m_programmeID;
	}
}
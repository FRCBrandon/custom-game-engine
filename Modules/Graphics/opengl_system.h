#pragma once
#ifndef OPENGL_SYSTEM_H
#define OPENGL_SYSTEM_H

#include "graphics_system.h"
#include "Modules/Graphics/Abstractions/camera.h"
#include "Modules/Graphics/Abstractions/light.h"
#include "Modules/Graphics/Mesh/mesh.h"
#include "Modules/Graphics/Shader/shaderprogramme.h"
#include "Modules/Graphics/Components/camera_component.h"
#include "Modules/Graphics/Components/mesh_component.h"
#include "Modules/Graphics/Components/mesh_renderer_component.h"
#include "Modules/Graphics/Components/light_component.h"

namespace Chin
{
	class OpenGLSystem : public GraphicsSystem
	{
	public:
		unsigned int VAO;

		void Initialize() final
		{
			GLInit();

			stbi_set_flip_vertically_on_load(true);

			InitializeShaderProgrammes();

			//InitializeFlyweights();

			m_standardProgramme.Use();
			m_standardProgramme.SetInt("material.diffuse", 0);
			m_standardProgramme.SetInt("material.specular", 1);
			m_standardProgramme.SetFloat("material.shininess", 32.0f);

			RegisterComponent(&m_cameraComponent);
			RegisterComponent(&m_meshComponent);
			RegisterComponent(&m_meshRendererComponent);
			RegisterComponent(&m_lightComponent);

			m_width = 800;
			m_height = 600;
		}

		void Shutdown() final
		{

		}

		void StepAll(real) final
		{
			m_meshRendererComponent.StepAll(0.0);
			m_lightComponent.StepAll(0.0);
		}

		void Render();

		void Resize(const unsigned int width, const unsigned int height)
		{
			// Set width and height
			m_width = float(width);
			m_height = float(height);

			// Adjust view port to new size
			glViewport(0, 0, width, height);

			// Set projection matrix
			m_projection = glm::perspective(m_currentCamera->GetFOVRadians(), m_width / m_height, 0.1f, 100.0f);
		}

		void SetCamera(Camera* camera) final
		{
			m_currentCamera = camera;
		}

		Light** SetLight(GenericLight& light) final
		{
			if(auto lightPtr = std::get_if<DirectionalLight>(&light))
			{
				return static_cast<Light**>(SetDirectionalLight(lightPtr));
			}
			else if(auto lightPtr = std::get_if<PointLight>(&light))
			{
				return static_cast<Light**>(SetPointLight(lightPtr));
			}
			else if(auto lightPtr = std::get_if<SpotLight>(&light))
			{
				return static_cast<Light**>(SetSpotLight(lightPtr));
			}
		}
	private:
		const int NR_POINT_LIGHTS = 4;

		float m_width, m_height;

		glm::mat4 m_model, m_view, m_projection;
		GLint m_uniModel, m_uniView, m_uniProjection;
		GLint m_standardProgrammeID, m_lightProgrammeID;

		Camera* m_currentCamera = nullptr;

		DirectionalLight *m_dirLight = nullptr;
		std::array<PointLight*, 4> m_pointLights = { nullptr };
		SpotLight *m_spotLight = nullptr;

		ShaderProgramme m_standardProgramme, m_lightProgramme;

		typedef std::pair<ShaderProgramme*, std::vector<Drawable*>> DrawablesList;
		std::vector<DrawablesList> m_shaderProgrammes;

		CameraComponent m_cameraComponent;

		MeshComponent m_meshComponent;
		MeshRendererComponent m_meshRendererComponent;

		LightComponent m_lightComponent;

		void GLInit()
		{
			// Initialize GLEW(OpenGL 3.3+)
			glewExperimental = GL_TRUE;
			glewInit();

			// Initialize OpenGL states
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			glFrontFace(GL_CCW);

			// Set clear color
			glClearColor(0.6f, 0.6f, 0.8f, 1.0f);
		}

		void InitializeShaderProgrammes()
		{
			// Load light programme
			m_lightProgramme.Create();
			m_lightProgramme.Load("Resources/Shaders/light.programme");
			m_lightProgrammeID = m_lightProgramme.GetProgrammeID();

			// Load standard programme
			m_standardProgramme.Create();
			m_standardProgramme.Load("Resources/Shaders/std.programme");
			m_standardProgrammeID = m_standardProgramme.GetProgrammeID();
		}

		void UpdateDirectionalLight()
		{
			if(m_dirLight)
			{
				m_standardProgramme.SetDirectionalLight("dirLight", *m_dirLight);
			}
		}

		void UpdatePointLights()
		{
			for(auto i = 0; i < NR_POINT_LIGHTS; i++)
			{
				if(m_pointLights[i])
				{
					m_standardProgramme.SetPointLight("pointLights[" + std::to_string(i) + "]", *m_pointLights[i]);
				}
			}
		}

		void UpdateSpotLight()
		{
			if(m_spotLight)
			{
				m_standardProgramme.SetSpotLight("spotLight", *m_spotLight);
			}
		}

		void UpdateLights()
		{
			UpdateDirectionalLight();
			UpdatePointLights();
			UpdateSpotLight();
		}

		void* SetDirectionalLight(DirectionalLight* light)
		{
			m_dirLight = light;

			return &m_dirLight;
		}

		void* SetPointLight(PointLight* light)
		{
			for(auto& pointLight : m_pointLights)
			{
				if(!pointLight)
				{
					pointLight = light;
					return &pointLight;
				}
			}

			m_pointLights[0] = light;
			return &m_pointLights[0];
		}

		void* SetSpotLight(SpotLight* light)
		{
			m_spotLight = light;

			return &m_spotLight;
		}

		ShaderProgramme& GetStandardProgramme()
		{
			return m_standardProgramme;
		}
	};
}

#endif
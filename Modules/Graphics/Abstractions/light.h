#pragma once
#ifndef LIGHT_H
#define LIGHT_H

#include <glm/vec3.hpp>
#include <glm/trigonometric.hpp>
#include <variant>

namespace Chin
{
	// Constant to make sure Light doesn't Get dimmer when close to object
	const float ATTENNUATED_LIGHT_CONSTANT = 1.0f;

	// Holds base variables pertaining to Shader's Light structs
	struct Light
	{
	public:
		Light() = default;

		// Holds the type of Light
		enum class Type { Error = -1, Generic, Directional, Point, Spot } m_type = Type::Error;

		Light(Type type) : m_type(type) {}

		glm::vec3 m_ambient = { 1.0f, 0.5f, 0.31f };
		glm::vec3 m_diffuse = { 1.0f, 0.5f, 0.31f };
		glm::vec3 m_specular = { 0.5f, 0.5f, 0.5f };
	};

	// Holds variables pertaining to Shader's Attenuated Light struct
	struct AttenuatedLight : public Light
	{
	public:
		float m_constant = ATTENNUATED_LIGHT_CONSTANT; // Constant to make sure Light doesn't Get dimmer when close to object
		float m_linear = 0.09f;
		float m_quadratic = 0.032f;
	};

	// Holds variables pertaining to Shader's DirectionalLight struct
	struct DirectionalLight : public Light
	{
	public:
		glm::vec3 m_direction; // Direction of Light

		// Constructs DirectionalLight
		DirectionalLight() { m_type = Light::Type::Directional; }

		// Constructs DirectionalLight with direction
		DirectionalLight(glm::vec3 direction) : m_direction(direction) { m_type = Light::Type::Directional; }
	};

	// Holds variables pertaining to Shader's PointLight struct
	struct PointLight : public AttenuatedLight
	{
	public:
		glm::vec3 m_position; // Position of Light

		// Constructs PointLight
		PointLight() { m_type = Light::Type::Point; }

		// Constructs PointLight with position
		PointLight(glm::vec3 position) : m_position(position) { m_type = Light::Type::Point; }
	};

	// Holds variables pertaining to Shader's SpotLight struct
	struct SpotLight : public AttenuatedLight
	{
	public:
		glm::vec3 m_position; // Position of Light
		glm::vec3 m_direction; // Direction of Light
		float m_cutoff = glm::cos(glm::radians(12.5f)); // Cutoff of SpotLight
		float m_outerCutoff = glm::cos(glm::radians(17.5f)); // Soft cutoff of SpotLight

		// Constructs SpotLight
		SpotLight() { m_type = Light::Type::Spot; }

		// Constructs SpotLight with position and direction
		SpotLight(glm::vec3 position, glm::vec3 direction) : m_position(position), m_direction(direction) { m_type = Light::Type::Point; }
	};

	// Any of the three Lights Directional, Point, and Spot
	//typedef std::variant<DirectionalLight*, PointLight*, SpotLight*> GenericLight;

	/*
	struct GenericLight : public SpotLight
	{
	public:
		GenericLight() {}

		void SwitchTo(Light::Type type)
		{
			ASSERT(!(type == 0 || type == -1));
			m_type = type;
		}

		bool isDirectionalLight()
		{
			if(m_type == Light::Type::Directional)
				return true;
			return false;
		}

		bool isPointLight()
		{
			if(m_type == Light::Type::Point)
				return true;
			return false;
		}

		bool isSpotLight()
		{
			if(m_type == Light::Type::Spot)
				return true;
			return false;
		}

		DirectionalLight GetDirectionalLight()
		{
			if(isDirectionalLight())
			{
				DirectionalLight light;
				light.m_ambient = m_ambient;
				light.m_diffuse = m_diffuse;
				light.m_specular = m_specular;
				light.m_direction = m_direction;

				return light;
			}
		}

		PointLight& GetPointLight()
		{
		}

		SpotLight& GetSpotLight()
		{
		}
	};*/

	using GenericLight = std::variant<DirectionalLight, PointLight, SpotLight>;
}

#endif // LIGHT_H

#include "camera.h"

namespace Chin
{
	void Camera::Translate(Direction dir, float dt)
	{
	    GLfloat camspeed = m_speed * (dt);

		glm::vec3 lockedPosition = m_cameraPosition;

	    if(dir == Direction::FORWARD)
	        m_cameraPosition += camspeed * m_cameraFront;
	    if(dir == Direction::BACK)
	        m_cameraPosition -= camspeed * m_cameraFront;
	    if(dir == Direction::LEFT)
	        m_cameraPosition -= glm::normalize(glm::cross(m_cameraFront, m_cameraUp)) * camspeed;
	    if(dir == Direction::RIGHT)
	        m_cameraPosition += glm::normalize(glm::cross(m_cameraFront, m_cameraUp)) * camspeed;

	    if(m_locked[Lock::POSITION_X])
	        m_cameraPosition.x = lockedPosition.x;
	    if(m_locked[Lock::POSITION_Y])
	        m_cameraPosition.y = lockedPosition.y;
	    if(m_locked[Lock::POSITION_Z])
	        m_cameraPosition.z = lockedPosition.z;
	}

	void Camera::Look(glm::vec2 delta)
	{
	    GLfloat xoffSet = delta.x * m_sensitivity;
	    GLfloat yoffSet = delta.y * m_sensitivity;

		m_rotation.y += xoffSet;
		m_rotation.x += yoffSet;

	    if(m_rotation.x > 89.0f)
			m_rotation.x = 89.0f;
	    if(m_rotation.x < -89.0f)
			m_rotation.x = -89.0f;

	    glm::vec3 front;
	    if(!m_locked[Lock::ROTATION_X])
	        front.x = cos(glm::radians(m_rotation.x)) * cos(glm::radians(m_rotation.y));
	    if(!m_locked[Lock::ROTATION_Y])
	        front.y = sin(glm::radians(m_rotation.x));
	    if(!m_locked[Lock::ROTATION_Z])
	        front.z = cos(glm::radians(m_rotation.x)) * sin(glm::radians(m_rotation.y));
		m_cameraFront = glm::normalize(front);
	}

	void Camera::Look(short dx, short dy)
	{
		Look(glm::vec2(dx, dy));
	}

	void Camera::Zoom(float delta)
	{
	    if(m_fov >= 1.0f && m_fov <= 45.0f)
			m_fov -= delta;
	    if(m_fov < 1.0f)
			m_fov = 1.0f;
	    if(m_fov > 45.0f)
			m_fov = 45.0f;
	}

	glm::mat4 LookAt(Camera& camera)
	{
		return glm::lookAt(camera.m_cameraPosition, camera.m_cameraPosition + camera.m_cameraFront, camera.m_cameraUp);
	}
}
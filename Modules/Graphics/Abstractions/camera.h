#pragma once
#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>

#include <bitset>

namespace Chin
{
	class Camera
	{
	public:
		// Used to lock directions/rotations
		enum Lock : char { POSITION_X, POSITION_Y, POSITION_Z, ROTATION_X, ROTATION_Y, ROTATION_Z, UNUSED_1, UNUSED_2 };
	protected:
		std::bitset<8> m_locked; // Holds info on which directions/rotations are locked

		// x = pitch, y = yaw, z = roll
		glm::vec3 m_rotation = glm::vec3(0.0f, 0.0f, 0.0f);

	    GLfloat m_fov = 45.0f, // FOV of camera
			m_speed = 10.0f, // Speed of camera
			m_sensitivity = 0.10f; // Sensitivity of camera
	public:
		Camera() = default;

	    glm::vec3 m_cameraPosition = glm::vec3(0.0f, 0.0f, 0.0f), // Position of camera
			m_cameraFront = glm::vec3(0.0f, 0.0f, -1.0f), // Direction forward from the camera
			m_cameraUp = glm::vec3(0.0f, 1.0f, 0.0f); // Direction upwards of the camera

		enum class Direction : char { FORWARD, BACK, LEFT, RIGHT };

		// Move camera in direction relative to front of camera
		void Translate(Direction dir, float dt);

		// Adjust direction the camera Is pointing
	    void Look(glm::vec2 delta);

		// Adjust direction the camera Is pointing
		void Look(short dx, short dy);

		// Magnify/Minify image by adjusting FOV
	    void Zoom(float delta);

		// Toggle lock property
		void TogglePropertyLock(Lock property) { m_locked[property] = !m_locked[property]; }

		// Set lock property
		void SetPropertyLock(Lock property, bool value) { m_locked[property] = value; }

		// Lock property
		void LockProperty(Lock property) { m_locked[property] = true; }

		// Unlock property
		void UnlockProperty(Lock property) { m_locked[property] = false; }

		// Return true if property Is locked
		bool IsPropertyLocked(Lock property) { return m_locked[property]; }

		// Return true if any property Is locked
		bool HasPropertyLocked() { return m_locked.any(); }

		void SetPitch(GLfloat pitch) { m_rotation.x = pitch; }
		GLfloat GetPitch() { return m_rotation.x; }

		void SetYaw(GLfloat yaw) { m_rotation.y = yaw; }
		GLfloat GetYaw() { return m_rotation.y; }

		void SetRoll(GLfloat roll) { m_rotation.z = roll; }
		GLfloat GetRoll() { return m_rotation.z; }

		void SetRotation(glm::vec3 rotation) { m_rotation = rotation; }
		glm::vec3 GetRotation() { return m_rotation; }

	    void SetFOV(GLfloat degrees) { m_fov = glm::radians(degrees); }
		void SetFOVRadians(GLfloat radians) { m_fov = radians; }
		GLfloat GetFOV() { return m_fov; }
		GLfloat GetFOVRadians() { return glm::radians(m_fov); }

	    void SetSpeed(GLfloat velocity) { m_speed = velocity; }
		GLfloat GetSpeed() { return m_speed; }

	    void SetSensitivity(GLfloat sensitivity) { m_sensitivity = sensitivity; }
		GLfloat GetSensitivity() { return m_sensitivity; }

	    void SetPosition(glm::vec3 position) { m_cameraPosition = position; }
		glm::vec3 GetPosition() { return m_cameraPosition; };

		void SetFront(glm::vec3 front) { m_cameraFront = front; }
		glm::vec3 GetFront() { return m_cameraFront; };

		void SetUp(glm::vec3 up) { m_cameraUp = up; }
		glm::vec3 GetUp() { return m_cameraUp; };
	};

	glm::mat4 LookAt(Camera& camera);
}

#endif // CAMERA_H

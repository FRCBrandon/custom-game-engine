#pragma once
#ifndef MATERIAL_H
#define MATERIAL_H

#include <glm/vec3.hpp>
#include <GL/glew.h>

namespace Chin
{
	// Unused
	struct Material
	{
		glm::vec3 m_ambient;
		glm::vec3 m_diffuse;
		glm::vec3 m_specular;
		GLfloat m_shininess;
	};

	// Unused
	struct SamplerMaterial
	{
		GLfloat m_shininess;
	};
}

#endif // MATERIAL_H

#include "Modules/Graphics/Classes/Meshes/flyweight.h"

namespace Chin
{
	void Flyweight::Render()
	{
	    mesh->Bind();
	    for(auto it : transforms)
	    {
	        glm::mat4 model;
	
	        model = glm::translate(model, Toglm(it->getPosition()));
	        model = glm::scale(model, Toglm(it->getScale()));
	        //model = glm::rotate(model, transform.getRotation());
	        glUniformMatrix4fv(model_proj, 1, GL_FALSE, glm::value_ptr(model));
	
	        mesh->Draw();
	    }
	    mesh->Unbind();
	}
	
	void Flyweight::addTransform(Transform* transform)
	{
	    transforms.push_back(std::shared_ptr<Transform>(transform));
	}
	
	void Flyweight::addTransform(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation)
	{
	    transforms.push_back(std::make_shared<Transform>(position, scale, rotation));
	}
}

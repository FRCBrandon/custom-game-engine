#pragma once
#ifndef VERTEX_H
#define VERTEX_H

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

namespace Chin
{
	struct Vertex
	{
		glm::vec3 m_position = { 0.0f, 0.0f, 0.0f };	// 12 bytes
		glm::vec3 m_normal = { 0.0f, 0.0f, 0.0f };		// 12 bytes
		glm::vec2 m_texcoords = { 0.0f, 0.0f };			// 8 bytes
														//32 bytes
		bool operator==(const Vertex& vertex)
		{
			if (m_normal == vertex.m_normal && m_position == vertex.m_position && m_texcoords == vertex.m_texcoords)
				return true;
			else
				return false;
		}
	};
}

#endif // VERTEX_H
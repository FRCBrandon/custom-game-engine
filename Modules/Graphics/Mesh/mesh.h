#pragma once
#ifndef MESH_H
#define MESH_H

#include "Core/core.h"

#include "Modules/Physics/Structures/vector3.h"

#include "drawable.h"
#include "vertex.h"
#include "texture.h"
#include "material.h"

#include "tiny_obj_loader.h"
#include "stb_image.h"

#include <vector>
#include <iostream>
#include <sstream>
#include <array>

namespace Chin
{
	const unsigned short MAX_TEXTURES = 2;

	// Interface for vertices and indices and OpenGL
	class Mesh : public Drawable
	{
	public:
		//Mesh() {}

		// Construct Mesh from wavefront(.obj) file
	    Mesh(const char* objPath);

		// Construct Mesh from wavefront(.obj) file
		Mesh(const char* objPath, const char* texturePath);

		Mesh(const Mesh&) = delete;

		// Mesh move constructor
		Mesh(Mesh&& rvalue);

		~Mesh()
		{
			DestroyMesh();
		}

		// Loads texture into texture in slot id
		void LoadTexture(const char* filePath, GLuint id);

		// Loads wavefront (.obj) file into Mesh
		void LoadVertices(const char* filePath);

		// Binds VAO and textures
		void Bind();
		// Draw Elements with indices
	    void Draw();
		// Unbinds VAO
	    void Unbind();

		// Bind VAO, Draw, and Unbind VAO
	    void Render();

		// Get VAO
		GLuint GetVAO() { return m_vao; }
		// Get VBO
		GLuint GetVBO() { return m_vbo; }
		// Get EBO
		GLuint GetEBO() { return m_ebo; }
		// Get Texture
		GLuint GetTexture(GLuint id)
		{
			if(id < MAX_TEXTURES)
				return m_textures[id];
			return 0;
		}
	private:
		GLuint m_vao = 0, m_vbo = 0, m_ebo = 0; // Used to access OpenGL

		std::vector<Vertex> m_vertices; // Holds vertices to put in Buffer
		std::vector<unsigned int> m_indices; // Holds indices to put in Buffer

		std::array<GLuint, MAX_TEXTURES> m_textures = {0};

		// Create mesh VBOs and VAOs
		void SetupMesh();

		// Destroy mesh VBOs and VAOs
		void DestroyMesh();
	};

}

#endif // MESH_H

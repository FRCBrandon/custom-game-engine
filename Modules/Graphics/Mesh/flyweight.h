#pragma once
#ifndef FLYWEIGHT_H
#define FLYWEIGHT_H

#include "Modules/Graphics/Classes/Meshes/mesh.h"
#include "Core/Structs/Data/transform.h"
#include <memory>

namespace Chin
{
	// Flyweight pattern for Meshes
	class Flyweight
	{
	public:
		// Vector of Transform data
	    std::vector<std::shared_ptr<Transform>> transforms;
	private:
	    GLint model_proj; // Model Projection to render to
	    std::shared_ptr<Mesh> mesh; // Pointer to Mesh
	public:
		// Constructs FLyweight
	    Flyweight(GLint& proj, Mesh* source) : model_proj(proj), mesh(source) {}
	
		// Renders Flyweight
	    void Render();
	
		// Adds Transform data to Flyweight
	    void addTransform(Transform* transform);
	
		// Adds Transform data to Flyweight
	    void addTransform(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation);
	};
}

#endif // FLYWEIGHT_H

#pragma once
#ifndef DRAWABLE_H
#define DRAWABLE_H

namespace Chin
{
	class Drawable
	{
	public:
		virtual void Render() = 0;
	};
}

#endif // DRAWABLE_H

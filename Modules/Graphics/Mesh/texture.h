#pragma once
#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <GL/glew.h>

namespace Chin
{
	struct Texture
	{
		GLuint m_id;
		std::string m_type;
	};
}

#endif // TEXTURE_H
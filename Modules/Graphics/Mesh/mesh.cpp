#include "mesh.h"

namespace Chin
{
	Mesh::Mesh(const char* objPath)
	{
		// Load vertices from file into array of vertices
		LoadVertices(objPath);
		// Initialize mesh
		SetupMesh();
	}

	Mesh::Mesh(const char* objPath, const char* texturePath)
	{
		// Load vertices from file into array of vertices
		LoadVertices(objPath);
		// Initialize mesh
		SetupMesh();
		// Load texture
		LoadTexture(texturePath, 0);
	}

	Mesh::Mesh(Mesh&& rvalue) :
		m_vao(rvalue.m_vao),
		m_vbo(rvalue.m_vbo),
		m_ebo(rvalue.m_ebo),
		m_vertices(std::move(rvalue.m_vertices)),
		m_indices(std::move(rvalue.m_indices)),
		m_textures(rvalue.m_textures)
	{
		rvalue.m_vao = 0;
		rvalue.m_vbo = 0;
		rvalue.m_ebo = 0;

		rvalue.m_textures = {0};
	}

	void Mesh::LoadTexture(const char* filePath, GLuint id)
	{
		// Generate Texture VBO
		glGenTextures(1, &m_textures[id]);

		// Bind Texture VBO
		glBindTexture(GL_TEXTURE_2D, m_textures[id]);

		// Set parameters for texture mapping (s,t)
		// Texture repeats along coordinate s
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		// Texture repeats along coordinate t
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// Texture uses nearest pixel for minification (scaled-down)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		// Texture uses linear filtering for magnification (scaled-up)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// Variables to be retrieved from image
		int width, height, nrChannels;
		// Retrieve data from image
		unsigned char *data = stbi_load(filePath, &width, &height, &nrChannels, 0);

		// If image Is succesfully loaded
		if (data)
		{
			// Default image format Is RGBA
			auto imageFormat = GL_RGBA;

			// If image Has three channels
			if(nrChannels == 3)
			{
				// Format Has no alpha channel
				imageFormat = GL_RGB;
			}

			// Generate the texture on the bound texture object at the active texture unit
			glTexImage2D(GL_TEXTURE_2D, 0, imageFormat, width, height, 0, imageFormat, GL_UNSIGNED_BYTE, data);

			// Generate mipmap for texture
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else
		{
			Console::Error( "Failed to load texture" );
		}

		// Free file
		stbi_image_free(data);
	}

	void Mesh::LoadVertices(const char* filePath)
	{
		tinyobj::attrib_t attrib;
		std::vector<tinyobj::shape_t> shapes;
		std::vector<tinyobj::material_t> materials;

		// String to hold error message
		std::string err;
		bool result = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, filePath);


		if (!err.empty())
		{
			Console::Error( err );
		}
		if (!result)
		{
			exit(1);
		}

		std::vector<Vertex> temp_vertices;
		//temp_vertices.reserve(attrib.vertices.size());

		// makes vertices out of indices in obj
		// loop over shapes
		for (size_t s = 0; s < shapes.size(); s++)
		{
			// loop over faces
			size_t index_offSet = 0;
			for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++)
			{
				size_t fv = shapes[s].mesh.num_face_vertices[f];
				// loop over vertices in the face, repeats 3 times before exiting
				for (size_t v = 0; v < fv; v++)
				{
					tinyobj::index_t idx = shapes[s].mesh.indices[index_offSet + v];
					Vertex vertex;
					vertex.m_position.x = attrib.vertices[3 * idx.vertex_index + 0];
					vertex.m_position.y = attrib.vertices[3 * idx.vertex_index + 1];
					vertex.m_position.z = attrib.vertices[3 * idx.vertex_index + 2];
					if (!attrib.normals.empty())
					{
						vertex.m_normal.x = attrib.normals[3 * idx.normal_index + 0];
						vertex.m_normal.y = attrib.normals[3 * idx.normal_index + 1];
						vertex.m_normal.z = attrib.normals[3 * idx.normal_index + 2];
					}
					if (!attrib.texcoords.empty())
					{
						vertex.m_texcoords.x = attrib.texcoords[2 * idx.texcoord_index + 0];
						vertex.m_texcoords.y = attrib.texcoords[2 * idx.texcoord_index + 1];
					}
					temp_vertices.push_back(vertex);
				}
				// offSet by num of vertex per face (3 times)
				index_offSet += fv;

				// shapes[s].mesh.material_ids[f];
			}
		}

		//m_vertices.reserve(temp_vertices.size());

		// move temporary vertices into vertices with indices
		for (size_t a = 0; a < temp_vertices.size(); a++)
		{
			bool inVertices = false;
			for (size_t b = 0; b < m_vertices.size(); b++)
			{
				if (temp_vertices[a] == m_vertices[b])
				{
					m_indices.push_back(b); // add index of vertex to index
					inVertices = true;
					break; // break b loop
				}
			}
			if (!inVertices)
			{
				m_vertices.push_back(temp_vertices[a]);
				m_indices.push_back(m_vertices.size() - 1);
			}
		}

		//for (auto& it : materials)
		//{
		//	Material temporary;
		//	temporary.ambient = it.ambient;
		//	temporary.diffuse = it.diffuse;
		//	temporary.specular = it.specular;
		//	temporary.shininess = it.shininess;
		//}
	}

	void Mesh::SetupMesh()
	{
		// Generate VAO
	    glGenVertexArrays(1, &m_vao);

		// Generate VBOs
	    glGenBuffers(1, &m_vbo);
	    glGenBuffers(1, &m_ebo);

		// Bind VAO
	    glBindVertexArray(m_vao);

		// Bind Array Buffer VBO
	    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

		// Pass vertices to Array Buffer VBO
	    glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(Vertex), &m_vertices[0], GL_STATIC_DRAW);

		// Bind indices Array Buffer VBO
	    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);

		// Pass indices Indices Array Buffer VBO
	    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(unsigned int), &m_indices[0], GL_STATIC_DRAW);

	    // first three floats are vertex positions
	    glEnableVertexAttribArray(0);
	    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(0));

	    // second three floats are vertex normals
	    glEnableVertexAttribArray(1);
	    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3)));

	    // Last two floats are vertex texture coords
	    glEnableVertexAttribArray(2);
	    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3) + sizeof(glm::vec3)));

		// Unbind VAO
	    glBindVertexArray(0);
	}

	void Mesh::DestroyMesh()
	{
		glDeleteTextures(MAX_TEXTURES, &m_textures[0]);

		glDeleteBuffers(1, &m_ebo);
		glDeleteBuffers(1, &m_vbo);

		glDeleteVertexArrays(1, &m_vao);
	}

	void Mesh::Bind()
	{
		// Bind Textures
		for (auto i = 0; i < MAX_TEXTURES; i++)
		{
			// Set active texture to i
			glActiveTexture(GL_TEXTURE0 + i);

			// Bind texture vbo to active texture
			glBindTexture(GL_TEXTURE_2D, m_textures[i]);
		}

	    // Bind VAO
	    glBindVertexArray(m_vao);
	}

	void Mesh::Draw()
	{
		// Draw mesh
	    glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, 0);
	}

	void Mesh::Unbind()
	{
	    glBindVertexArray(0);
	}

	void Mesh::Render()
	{
	    Bind();
	    Draw();
	    Unbind();
	}
}

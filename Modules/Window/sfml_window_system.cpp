#include "sfml_window_system.h"
#include "Core/game_engine.h"

namespace Chin
{
	void SFMLWindowSystem::StepAll(real deltaTime)
	{
		// while window is polling events
		if(m_window.pollEvent(m_event))
		{
			// if event request close
			if(m_event.type == sf::Event::Closed)
			{
				// quit
				m_gameEngine.Quit();
			}
			// if event is resized
			else if(m_event.type == sf::Event::Resized)
			{
				// resize renderer
				m_gameEngine.GetGraphicsSystem().Resize(m_event.size.width, m_event.size.height);
			}
			// if key states change
			else if(m_event.type == sf::Event::KeyPressed || m_event.type == sf::Event::KeyReleased)
			{
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				{
					// unlock mouse
					m_inputSystem.UnlockMouse();

					// quit
					m_gameEngine.Quit();
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Tilde))
				{
					// unlock mouse
					m_inputSystem.UnlockMouse();

					std::string input;
					do
					{
						// retrieve script
						std::getline(std::cin, input);

						// execute script
						m_gameEngine.GetScriptSystem().ExceuteScript(input);
					} while(input != std::string());

					// lock mouse
					m_inputSystem.LockMouse(GetWindowWidth() / 2, GetWindowHeight() /2);
					m_window.requestFocus();
				}
				// retrieve m_keys
				m_inputSystem.RetrieveKeys();
			}
			else if(m_event.type == sf::Event::MouseMoved)
			{
				// retrieve mouse
				m_inputSystem.RetrieveMouse();
			}
			else if(m_event.type == sf::Event::MouseWheelScrolled)
			{
				// Set delta scroll
				m_inputSystem.SetDeltaScroll(m_event.mouseWheelScroll.delta);
			}
		}
	}
}
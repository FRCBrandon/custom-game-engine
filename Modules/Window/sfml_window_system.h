#pragma once
#ifndef SFML_WINDOW_SYSTEM_H
#define SFML_WINDOW_SYSTEM_H

#include "window_system.h"
#include "Modules/Input/sfml_input_system.h"

namespace Chin
{
	class GameEngine;

	class SFMLWindowSystem : public WindowSystem
	{
	public:
		struct Settings;

		SFMLWindowSystem(const Settings& Settings, GameEngine& gameEngine) :
			m_window(sf::VideoMode(Settings.width, Settings.height), Settings.title.c_str(), Settings.style, Settings.contextSettings),
			m_inputSystem(m_window),
  			m_gameEngine(gameEngine)
		{
		}

		void Initialize()
		{
			m_window.setVerticalSyncEnabled(false);
			m_window.setMouseCursorGrabbed(true);
			m_window.setMouseCursorVisible(false);
			sf::Mouse::setPosition(sf::Vector2i(GetWindowWidth() / 2, GetWindowHeight() / 2), m_window);
		}

		void Shutdown()
		{
		}

		void StepAll(real deltaTime) final;

		InputSystem& GetInputSystem() final
		{
			return m_inputSystem;
		}

		void Display() final
		{
			m_window.display();
		}

		unsigned int GetWindowWidth()
		{
			return m_window.getSize().x;
		}

		unsigned int GetWindowHeight()
		{
			return m_window.getSize().y;
		}
	private:
		sf::Window m_window;
		sf::Event m_event;

		SFMLInputSystem m_inputSystem;
		GameEngine& m_gameEngine;
	public:
		struct Settings
		{
			unsigned int width, height;
			const std::string title;
			sf::Uint32 style;
			const sf::ContextSettings& contextSettings;
		};
	};
}

#endif
#pragma once
#ifndef WINDOW_SYSTEM_H
#define WINDOW_SYSTEM_H

#include "Modules/Input/input_system.h"

namespace Chin
{
	class WindowSystem : public SystemBase
	{
	public:
		void PollEvent()
		{
			StepAll(0.0);
		}

		virtual InputSystem& GetInputSystem() = 0;

		virtual void Display() = 0;

		virtual unsigned int GetWindowWidth() = 0;
		virtual unsigned int GetWindowHeight() = 0;
	};
}

#endif
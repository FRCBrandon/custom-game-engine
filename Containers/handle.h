#pragma once
#ifndef HANDLE_H
#define HANDLE_H

namespace Chin
{
	struct Handle
	{
		uint16_t m_index = 0; // the position of the data
		uint16_t m_counter = 0; // amount of reuses of the handle

		Handle() = default;

		Handle(uint16_t index, uint16_t counter = 0) : m_index(index), m_counter(counter) {}

		bool operator==(const Handle& handle)
		{
			if ((this->m_counter == handle.m_counter) && (this->m_index == handle.m_index))
				return true;
			return false;
		}
	};
}

#endif
#pragma once
#ifndef DYNAMIC_HANDLE_H
#define DYNAMIC_HANDLE_H

#include "Allocators/dynamic_contiguous_allocator.h"
#include "handle.h"
#include <queue>

namespace Chin
{
	template<typename T>
	class DynamicHandle
	{
	public:
		struct Iterator;

		DynamicHandle(uint16_t reservedSize = 1024) : m_elements(sizeof(T), reservedSize, std::numeric_limits<uint16_t>::max())
		{
			ASSERT(reservedSize < pow(2, 16));
			m_handles.reserve(reservedSize);
			m_indices.reserve(reservedSize);
		}

		~DynamicHandle()
		{
			for (uint16_t i = m_elements.GetAmountOfElementsAllocated(); i > 0; i--)
			{
				DestructElement(i - 1);
			}
		}

		// Constructs element within handle
		// Beware of calling copy/move constructors
		// Throws exception if allocation fails
		template<typename... Args>
		Handle ConstructElement(Args&&... args)
		{
			// Allocate space in allocator and construct T with placement new
			new (m_elements) T(args...);

			// Pass new element's index to retrieve it's handle
			return retrieveOpenHandle(m_elements.GetAmountOfElementsAllocated() - 1);
		}

		void DestructElement(Handle handle)
		{
			Handle& selectedHandle = m_handles[handle.m_index];
			if (selectedHandle.m_counter == handle.m_counter)
			{
				selectedHandle.m_counter++;

				const uint16_t handleIndexToEntry = handle.m_index;

				// Push deleted handle's index into list of free handles
				m_freeHandles.push(handleIndexToEntry);

				const uint16_t selectedIndexToElement = selectedHandle.m_index;

				T* selectedElement = static_cast<T*>(m_elements[selectedIndexToElement]);

				AllocatorDelete(selectedElement, m_elements);

				const uint16_t lastElementPosition = m_elements.GetAmountOfElementsAllocated();

				// NOT NEEDED?
				// Set element in selected position to last element
				// Deletes old element in proccess
				//selectedElement = m_elements[lastElementPosition];

				// Set index in selected postion to last element's position
				// Make sure the element Is still pointing to the right Handle
				m_indices[selectedIndexToElement] = m_indices[lastElementPosition];

				// Set handle's index in last element's position to the seleted position
				// Allows the handle to find new position of element
				m_handles[m_indices[selectedIndexToElement]].m_index = selectedIndexToElement;
			}
		}

		// Could be optimized??
		void DestructElement(uint16_t position)
		{
			if (position < m_elements.GetAmountOfElementsAllocated())
			{
				DestructElement(Handle(m_indices[position], m_handles[m_indices[position]].m_counter));
			}
		}

		T* Get(Handle handle)
		{
			// handle: index = 1022, counter = 1
			Handle entry = m_handles[handle.m_index];
			// entry.counter = 0 causing skip
			// inspect grow
			// numbers repeat in index
			if (entry.m_counter == handle.m_counter)
			{
				return static_cast<T*>(m_elements.GetBlockAddress(entry.m_index));
			}
			return nullptr;
		}

		// T* Get(uint16_t position);

		Handle GetHandle(uint16_t position)
		{
			return m_handles[position];
		}

		uint16_t GetPosition(Handle handle)
		{
			return m_handles[handle.m_index].m_index;
		}

		uint16_t GetElementsAllocated()
		{
			return m_elements.GetAmountOfElementsAllocated();
		}

		bool HasGrown()
		{
			return m_elements.HasGrown();
		}

		void ResetGrown()
		{
			m_elements.ResetGrown();
		}

		struct Iterator
		{
			Iterator(T* address) : m_address(address) {}

			// prefix ++
			Iterator& operator++()
			{
				m_address += 1;
				return *this;
			}

			// postfix ++
			Iterator operator++(int)
			{
				Iterator temp(*this);
				operator++();
				return temp;
			}

			// prefix --
			Iterator& operator--()
			{
				m_address -= 1;
				return *this;
			}

			// postfix --
			Iterator operator--(int)
			{
				Iterator temp(*this);
				operator--();
				return temp;
			}

			T* Get() const
			{
				return m_address;
			}

			T* operator->() const
			{
				return m_address;
			}

			T& operator*() const
			{
				return *m_address;
			}

			inline bool operator==(const Iterator& iterator) const
			{
				if (m_address == iterator.m_address)
					return true;
				return false;
			}

			inline bool operator!=(const Iterator& iterator) const
			{
				return !operator==(iterator);
			}
		private:
			T* m_address;
		};

		Iterator Begin()
		{
			return Iterator(static_cast<T*>(m_elements.GetBeginOfAllocatedMemory()));
		}

		Iterator End()
		{
			return Iterator(static_cast<T*>(m_elements.GetEndOfAllocatedMemory()));
		}
	private:
		// handle of the elements
		std::vector<Handle> m_handles;

		// index's position corresponds with element's position
		// holds data that points to handle's position
		std::vector<uint16_t> m_indices;

		// array of data
		DynamicContiguousAllocator m_elements;

		std::queue<uint16_t> m_freeHandles;
	private:
		Handle retrieveOpenHandle(const uint16_t elementIndex)
		{
			Handle handle;
			if (!m_freeHandles.empty())
			{
				const uint16_t firstOpenHandleIndex = m_freeHandles.front();

				m_indices[elementIndex] = firstOpenHandleIndex;

				m_handles[firstOpenHandleIndex].m_index = elementIndex;

				handle = m_handles[firstOpenHandleIndex];

				m_freeHandles.pop();
			}
			else
			{
				handle.m_index = elementIndex;

				m_handles.push_back(handle);

				m_indices.push_back(m_handles.size() - 1);
			}
			return handle;
		}
	};
}

#endif
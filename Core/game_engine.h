#pragma once
#ifndef GAME_ENGINE_H
#define GAME_ENGINE_H

#include "ECS/entity_list.h"
#include "Modules/Common/Components/transformcomponent.h"
#include "Modules/Window/sfml_window_system.h"
#include "Modules/Physics/chin_physics_system.h"
#include "Modules/Graphics/opengl_system.h"
#include "Modules/Scripting/lua_system.h"

#define RANGE_MAX 50

namespace Chin
{
	class GameEngine
	{
	public:
		struct Settings
		{
			SFMLWindowSystem::Settings windowSettings;
		};

		GameEngine(const Settings& Settings) :
			m_transformComponent(g_defaultAmountOfEntities),
			m_windowSystem(Settings.windowSettings, *this),
			m_inputSystem(m_windowSystem.GetInputSystem())
		{}

		void Initialize()
		{
			Entity::GetComponentRegistry().RegisterComponent(&m_transformComponent);

			m_windowSystem.Initialize();
			m_inputSystem.Initialize();
			m_physicsSystem.Initialize();
			m_graphicsSystem.Initialize();
			m_scriptSystem.Initialize();
		}

		void Shutdown()
		{
			m_scriptSystem.Shutdown();
			m_graphicsSystem.Shutdown();
			m_physicsSystem.Shutdown();
			m_inputSystem.Shutdown();
			m_windowSystem.Shutdown();
		}

		void PreLoop()
		{
			m_inputSystem.LockMouse(m_windowSystem.GetWindowWidth() / 2, m_windowSystem.GetWindowHeight() / 2);

			// Head
			AddEntity();
			AddComponent("Particle Physics");
			auto a = m_selectedEntity->GetComponentData("Particle Physics");
			auto b = static_cast<ParticlePhysicsComponentData*>(a);
			b->SetMass(1.0);

			AddComponent("Mesh");
			AddComponent("Mesh Renderer");

			// Camera
			AddEntity();
			AddComponent("Camera");
			AddComponent("Basic Camera Movement");


			// Light
			AddEntity();
			auto c = m_selectedEntity->GetComponentData("Transform");
			auto d = static_cast<TransformComponentData*>(c);
			AddComponent("Light");

			// Box
			AddEntity();
			m_selectedEntity->SetName("Box");
			AddComponent("Mesh");
			AddComponent("Mesh Renderer");
			m_prefabRegistry.CreatePrefab(*m_selectedEntity);

			srand(time(NULL));

			const size_t g_amountOfBoxes = 10;

			// 1020
			for(size_t count = 0; count < g_amountOfBoxes; count++)
			{
				AddEntity("Box");
				auto transformData = static_cast<TransformComponentData*>(m_selectedEntity->GetComponentData("Transform"));
				transformData->Translate(vector3(rand() % RANGE_MAX - RANGE_MAX/2, rand() % RANGE_MAX - RANGE_MAX/2, rand() % RANGE_MAX - RANGE_MAX/2));
			}
		}

		void Loop()
		{
			PreLoop();

			sf::Time time;
			sf::Clock accumulator;

			while(m_isRunning)
			{
				// Poll Window
				m_windowSystem.PollEvent();

				// Set duration
				time = accumulator.restart();
				m_deltaTime = time.asSeconds();

				// Process Inputs
				m_inputSystem.PollInput(m_deltaTime);

				// Integrate Physics
				m_physicsSystem.Integrate(m_deltaTime);

				// Render
				m_graphicsSystem.Render();

				// Display
				m_windowSystem.Display();
			}

			PostLoop();
		}

		void PostLoop()
		{
		}

		void Quit()
		{
			m_isRunning = false;
		}

		GraphicsSystem& GetGraphicsSystem()
		{
			return m_graphicsSystem;
		}

		ScriptSystem& GetScriptSystem()
		{
			return m_scriptSystem;
		}

		void AddEntity()
		{
			m_selectedEntity = m_entities.AddEntity();
		}

		void AddEntity(const std::string& prefabName)
		{
			m_selectedEntity = m_entities.AddEntity();
			m_selectedEntity->UseComponentList(m_prefabRegistry.GetPrefab(prefabName)->second);
		}

		void AddComponent(const std::string& componentName)
		{
			m_selectedEntity->AddComponent(componentName);
		}

	private:
		bool m_isRunning = true;
		real m_deltaTime = 0.0;

		TransformComponent m_transformComponent;

		SFMLWindowSystem m_windowSystem;
		InputSystem& m_inputSystem; // References InputSystem in WindowSystem
		ChinPhysicsSystem m_physicsSystem;
		OpenGLSystem m_graphicsSystem;
		LuaSystem m_scriptSystem;

		PrefabRegistry m_prefabRegistry;
		EntityList m_entities;
		Entity* m_selectedEntity;
	};
}

#endif
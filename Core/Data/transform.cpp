#include "stdafx.h"
#include "transform.h"

namespace Chin
{
	void Transform::Translate(vector3 offSet)
	{
		m_position += offSet;
	}

	void Transform::Scale(real magnitude)
	{
		m_scale *= magnitude;
	}

	void Transform::Rotate(vector3 offSet)
	{
		m_rotation += offSet;
	}
}
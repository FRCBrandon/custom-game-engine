#pragma once
#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "Modules/Physics/Structures/vector3.h"

namespace Chin
{
	// Use for position, scale, and rotation vectors
	struct Transform
	{
		vector3 m_position, m_scale, m_rotation; // Vector for Transform

		// Constructs Transform
		Transform(vector3 position = { 0.0f, 0.0f, 0.0f }, vector3 scale = { 1.0f, 1.0f, 1.0f }, vector3 rotation = { 0.0f, 0.0f, 0.0f }) : m_position(position), m_scale(scale), m_rotation(rotation) {}

		// Translates position by 3d vector
		void Translate(vector3 offSet);

		// Scales current size by magnitude
		void Scale(real magnitude);

		// Rotate by 3d vector
		void Rotate(vector3 offSet);

		// Set position
		void SetPosition(vector3 position) { m_position = position; }
		// Get position
		vector3 GetPosition() const { return m_position; }

		// Set scale
		void SetScale(vector3 scale) { m_scale = scale; }
		// Get scale
		vector3 GetScale() const { return m_scale; }

		// Set rotation
		void SetRotation(vector3 rotation) { m_rotation = rotation; }
		// Get rotation
		vector3 GetRotation() const { return m_rotation; }
	};
}
#endif // TRANSFORM_H

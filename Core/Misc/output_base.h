#pragma once
#ifndef OUTPUT_BASE_H
#define OUTPUT_BASE_H

#include <array>

namespace Chin
{
	struct InputBase;

	struct OutputBase
	{
	public:
		virtual void LinkInput(InputBase* input) = 0;
		virtual void UnlinkInput(InputBase* input) = 0;
		virtual void* GetPtr() = 0;
	};
}

#endif
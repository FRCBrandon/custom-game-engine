#pragma once
#ifndef INPUT_BASE_H
#define INPUT_BASE_H

namespace Chin
{
	struct OutputBase;

	struct InputBase
	{
	public:
		// Functions to be used by Output for Relinks
		virtual void SetOutput(OutputBase* output) = 0; // Create link between input and output

		struct InvalidOutputException : public Exception
		{
			const char* what() const throw()
			{
				return "Input Has no valid output";
			}
		};
	};
}

#endif
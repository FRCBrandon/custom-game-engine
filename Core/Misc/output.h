#pragma once
#ifndef OUTPUT_H
#define OUTPUT_H

#include "output_base.h"
#include "input_base.h"

namespace Chin
{
	template<typename T>
	struct Output : OutputBase
	{};

	template<typename T, size_t N>
	struct LimitedOutput : public Output<T>
	{
	public:
		template<typename... Args>
		LimitedOutput(Args&&... args) : m_data(args...)
		{}

		~LimitedOutput()
		{
			for (auto& i : m_inputs)
			{
				if (i != nullptr)
				{
					i->SetOutput(nullptr);
				}
			}
		}

		void LinkInput(InputBase* input)
		{
			for (auto& i : m_inputs)
			{
				if (i == nullptr)
				{
					i = input;
					return;
				}
			}
		}

		void UnlinkInput(InputBase* input)
		{
			for (auto& i : m_inputs)
			{
				if (i == input)
				{
					i = nullptr;
					return;
				}
			}
		}

		void* GetPtr() final
		{
			return &m_data;
		}

		T& GetRef()
		{
			return m_data;
		}

		void Relink()
		{
			for (auto input : m_inputs)
			{
				if(input)
				{
					input->SetOutput(this);
				}
			}
		}

		T* operator->()
		{
			return &m_data;
		}

		T& operator*()
		{
			return m_data;
		}
	private:
		T m_data;
		std::array<InputBase*, N> m_inputs = {};
	};

	template<typename T>
	struct UnlimitedOutput : public Output<T>
	{
	public:
		template<typename... Args>
		UnlimitedOutput(Args&&... args) : m_data(args...)
		{}

		~UnlimitedOutput()
		{
			for(auto& it : m_inputs)
			{
				if(it != nullptr)
				{
					it->SetOutput(nullptr);
				}
			}
		}

		void LinkInput(InputBase* input)
		{
			m_inputs.push_back(input);
		}

		void UnlinkInput(InputBase* input)
		{
			for (auto& it : m_inputs)
			{
				if (it == input)
				{
					m_inputs.erase(it);
					return;
				}
			}
		}

		void* GetPtr() final
		{
			return &m_data;
		}

		T& GetRef()
		{
			return m_data;
		}

		void Relink()
		{
			for (auto i : m_inputs)
			{
				i->SetOutput(this);
			}
		}

		T* operator->()
		{
			return &m_data;
		}

		T& operator*()
		{
			return m_data;
		}
	private:
		T m_data;
		std::vector<InputBase*> m_inputs;
	};
}

#endif
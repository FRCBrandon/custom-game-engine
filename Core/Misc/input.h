#pragma once
#ifndef INPUT_H
#define INPUT_H

#include "input_base.h"
#include "output_base.h"

namespace Chin
{
	template<typename T>
	struct Output;

	template<typename T>
	struct Input : public InputBase
	{};

	template<typename T>
	struct RequiredInput : public Input<T>
	{
	public:
		RequiredInput() : m_output(nullptr), m_selfPtr(this), m_ptr(nullptr) {}

		RequiredInput(OutputBase* output) : m_output(output), m_selfPtr(this), m_ptr(nullptr)
		{
			SetOutput(output);
		}

		~RequiredInput()
		{
			if(m_output)
			{
				UnlinkToOutput();
			}
		}

		void SetOutput(OutputBase* output) final
		{
			if(m_output)
			{
				UnlinkToOutput();
			}

			m_output = output;
			if (m_output)
			{
				// if output Is holding type T
				if (dynamic_cast<Output<T>*>(m_output))
				{
					LinkToOutput(*m_output);
					return;
				}

				m_output = nullptr;

				throw InvalidOutputException();
			}
			// output Has been set to nullptr
		}

		// Set pointer to data equal to data from output
		void SynchronizeDataPtr()
		{
			m_ptr = static_cast<T*>(m_output->GetPtr());
		}

		// Set pointer to data equal to nullptr
		void DesynchronizeDataPtr()
		{
			m_ptr = nullptr;
		}

		void Relink()
		{
			if(m_output)
			{
				m_output->UnlinkInput(m_selfPtr);
				LinkToOutput(*m_output);
				m_selfPtr = this;
			}
		}

		// Throws exception if pointer to data is nullptr
		T* Get()
		{
			if (m_ptr)
			{
				return m_ptr;
			}

			throw InvalidOutputException();
		}
	private:
		OutputBase* m_output = nullptr; // Points to output
		InputBase* m_selfPtr = this; // Points to self
		T* m_ptr = nullptr; // Points to data

		// Link input to output
		// Link last until input/output is destructed or link is broken manually
		void LinkToOutput(OutputBase& output)
		{
			// point to output
			m_output = &output;

			// create link in output
			m_output->LinkInput(this);

			// point to data
			SynchronizeDataPtr();
		}

		// Manually breaks link between input and output if one exist
		void UnlinkToOutput()
		{
			m_output->UnlinkInput(this);
			m_output = nullptr;
			DesynchronizeDataPtr();
		}
	};

	template<typename T>
	struct SafeInput
	{
	};

	// Input that doesn't throw exceptions
	// If output doesn't exist component remains active
	template<typename T>
	struct OptionalInput
	{
	};
}

#endif
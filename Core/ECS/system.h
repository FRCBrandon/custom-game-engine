#pragma once
#ifndef SYSTEM_H
#define SYSTEM_H

#include "entity.h"

namespace Chin
{
	// Needed by GameEngine and SpecificComponents
	class SystemBase
	{
	public:
		virtual void Initialize() = 0;

		virtual void Shutdown() = 0;

		virtual void StepAll(real deltaTime) = 0;

		void RegisterComponent(ComponentBase* component)
		{
			m_componentRegistry.RegisterComponent(component);
		}
	private:
		static ComponentRegistry& m_componentRegistry;
	};

	class SystemDerived : public SystemBase
	{
	public:
		virtual void Initialize() = 0;

		virtual void Shutdown() = 0;

		void StepAll(real deltaTime) final
		{
			for (auto& it : m_components)
			{
				it->StepAll(deltaTime);
			}
		}
	private:
		std::vector<std::unique_ptr<ComponentBase>> m_components;
	};
}

#endif
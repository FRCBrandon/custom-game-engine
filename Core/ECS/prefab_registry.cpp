#include "prefab_registry.h"'
#include "entity.h"

namespace Chin
{
	void PrefabRegistry::CreatePrefab(Entity& source)
	{
		Prefab prefab;

		prefab.first = source.GetName();

		for(auto it : source.GetComponents())
		{
			prefab.second.push_back(it.first);
		}

		m_prefabs.push_back(prefab);
	}

	PrefabRegistry::Prefab* PrefabRegistry::GetPrefab(const std::string & prefabName)
	{
		for(auto& prefab : m_prefabs)
		{
			if(prefab.first == prefabName)
			{
				return &prefab;
			}
		}

		return nullptr;
	}
}
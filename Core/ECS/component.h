#pragma once
#ifndef COMPONENT_H
#define COMPONENT_H

#include "component_registry.h"
#include "entity.h"

namespace Chin
{
	// Defined in System
	template<typename T>
	class Component : public ComponentBase
	{
	public:
		Component(std::string componentName, ComponentType componentType, uint16_t reservedSize) : ComponentBase(componentName, componentType), m_data(reservedSize)
		{
			bool constexpr inheritsData = std::is_base_of<ComponentBase::Data, T>::value;
			STATIC_ASSERT(inheritsData);
		}

		~Component()
		{
			for(auto i = m_data.GetElementsAllocated() - 1; i >= 0 ; i--)
			{
				Entity* entity = m_data.Get(m_data.GetHandle(0))->GetParent();
				entity->DeleteComponent(GetName());
			}
		}

		void StepAll(real deltaTime) final
		{
			for (auto it = m_data.Begin(); it != m_data.End(); it++)
			{
				it->Fn(deltaTime);
			}
		}

		// NEED MORE EFFICIENCY
		// NEED TO HANDLE EXCEPTIONS
		Handle CreateData(Entity* parent) final;

		void DestroyData(Handle handle) final
		{
			const uint16_t lastElementIndex = m_data.GetElementsAllocated() - 1;

			if (m_data.GetPosition(handle) != lastElementIndex)
			{
				Handle movedElementHandle = m_data.GetHandle(lastElementIndex);
				m_data.DestructElement(handle);
				m_data.Get(movedElementHandle)->OnShift();
			}
			else
			{
				m_data.DestructElement(handle);
			}
		}

		Data* GetData(Handle handle) final
		{
			auto data = m_data.Get(handle);
			ASSERT(data);
			return data;
		}

		void* RetrieveVariablePtr(Handle handle, SID variableName) final
		{
			auto data = m_data.Get(handle);
			ASSERT(data);
			if(!data)
			{
				throw;
			}
			return data->GetVariablePtr(variableName);
		}

		std::unique_ptr<DataFieldBase> RetrieveDataField(Handle handle, SID variableName) final
		{
			auto data = m_data.Get(handle);
			ASSERT(data);
			if(!data)
			{
				throw;
			}
			return data->GetDataField(variableName);
		}
	private:
		DynamicHandle<T> m_data;
	};

	template<typename T>
	inline Handle Component<T>::CreateData(Entity * parent)
	{
		Handle handle;

		try
		{
			handle = m_data.ConstructElement(parent);
		}
		catch(Exception &e)
		{
			// Replace with inactivity and throw
			Console::Error(GetName());
			Console::Exception(e);

			throw e;
		}
		catch(std::exception &e)
		{
			Console::Error(GetName());
			Console::Exception(e);

			throw e;
		}

		if(m_data.HasGrown())
		{
			for(auto it = m_data.Begin(); it != m_data.End(); it++)
			{
				it->OnShift();
			}
			m_data.ResetGrown();
		}

		return handle;
	}
}

#endif // COMPONENT_H
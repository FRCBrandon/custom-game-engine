#pragma once
#ifndef DATA_FIELD_H
#define DATA_FIELD_H

#include "data_field_base.h"

namespace Chin
{
	template<typename T>
	struct DataField : public DataFieldBase
	{
	public:
		DataField(T& data) : m_data(&data) {}

		T* Get() { return m_data; }

		T* operator->() const
		{
			return m_data;
		}

		T& operator*() const
		{
			return *m_data;
		}
	private:
		T* m_data;
	};
}

#endif
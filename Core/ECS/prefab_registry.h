#pragma once
#ifndef PREFAB_REGisTRY_H
#define PREFAB_REGisTRY_H

#include "Core/core.h"

namespace Chin
{
	class Entity;
	class ComponentBase;

	class PrefabRegistry
	{
	public:
		using ComponentList = std::vector<ComponentBase*>;
		using Prefab = std::pair<std::string, ComponentList>;

		PrefabRegistry()
		{}

		void CreatePrefab(Entity& source);

		/*void DeletePrefab(const std::string& prefabName)
		{
		}*/

		Prefab* GetPrefab(const std::string& prefabName);
	private:
		std::vector<Prefab> m_prefabs;
	};
}

#endif
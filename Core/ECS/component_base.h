#pragma once
#ifndef COMPONENT_BASE_H
#define COMPONENT_BASE_H

#include "Core/core.h"
#include "Containers/dynamic_handle.h"
#include "data_field.h"
#include "Core/Misc/output.h"
#include "Core/Misc/input.h"
#include <optional>

namespace Chin
{
	class Entity;

	enum class ComponentType : char { Error = -1, Data = 0, Input = 1, Physics, Graphics };

	class ComponentBase
	{
	public:
		struct Data;

		ComponentBase(std::string componentName, ComponentType componentType) : m_name(componentName), m_type(componentType) {}
		~ComponentBase() = default;

		virtual void StepAll(real deltaTime) = 0;

		virtual Handle CreateData(Entity* parent) = 0;
		virtual void DestroyData(Handle handle) = 0;
		virtual Data* GetData(Handle handle) = 0;

		virtual void* RetrieveVariablePtr(Handle handle, SID variableName) = 0;

		virtual std::unique_ptr<DataFieldBase> RetrieveDataField(Handle handle, SID variableName) = 0;

		std::string GetName() { return m_name; }
		ComponentType GetType() { return m_type; }
	private:
		std::string m_name;
		ComponentType m_type;
	public:
		struct Data
		{
		public:
			Data(Entity* parent) : m_parent(parent) {}

			// Not Needed?
			virtual ~Data() {}

			virtual void Fn(real deltaTime) {}

			virtual void OnShift() {}

			virtual void* GetVariablePtr(SID variableName) { return nullptr; }

			virtual std::unique_ptr<DataFieldBase> GetDataField(SID variableName) { return nullptr; }

			// Called by command line and in ComponentData constructors
			//NEED FINiSH
			bool CreateLink(SID inputName, const std::string& componentName, SID outputName);

			Entity* GetParent() { return m_parent; }
		protected:
			Data* RequestComponent(const std::string& componentName);

			Data* RequireComponent(const std::string& componentName);

			// Need more efficiency
			template<typename T>
			T* RequestVariable(const std::string& componentName, SID variableName)
			{
				try
				{
					return RequireVariable<T>(componentName, variableName);
				}
				catch(Exception &e)
				{
					Console::Warning(e.what());
					return nullptr;
				}
			}

			template<typename T>
			T* RequestVariable(Data* data, SID variableName)
			{
				try
				{
					return RequireVariable<T>(data, variableName);
				}
				catch(Exception &e)
				{
					Console::Warning(e.what());
					return nullptr;
				}
			}

			template<typename T>
			T* RequireVariable(const std::string& componentName, SID variableName)
			{
				auto dataFieldBase = GetParent()->GetDataField(componentName, variableName);

				if(DataField<T>* dataFieldDerived = dynamic_cast<DataField<T>*>(dataFieldBase.get()))
				{
					return dataFieldDerived->Get();
				}

				throw UnavailableVariableException();
			}

			template<typename T>
			T* RequireVariable(Data* data, SID variableName)
			{
				auto dataFieldBase = data->GetDataField(variableName);

				if(DataField<T>* dataFieldDerived = dynamic_cast<DataField<T>*>(dataFieldBase.get()))
				{
					return dataFieldDerived->Get();
				}

				throw UnavailableVariableException();
			}

			template<typename T>
			std::unique_ptr<DataFieldBase> CreateDataField(T& variable) { return std::make_unique<DataField<T>>(variable); }
		private:
			Entity* m_parent;
		public:
			struct UnavailableComponentException : public Exception
			{
				const char* what() const throw() { return "Component not available"; }
			};

			struct UnavailableVariableException : public Exception
			{
				const char* what() const throw() { return "Variable not available"; }
			};
		};
	};
}

#endif // COMPONENT_BASE_H
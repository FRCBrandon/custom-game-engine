#pragma once
#ifndef COMPONENT_REGisTRY_H
#define COMPONENT_REGisTRY_H

#include "component_base.h"

namespace Chin
{
	// Needed by Entity and System
	class ComponentRegistry
	{
	public:
		void RegisterComponent(ComponentBase* component)
		{
			m_registrations.push_back(Registration{ component->GetName(), component });
		}

		ComponentBase* FindComponent(const std::string& componentName)
		{
			for (auto& it : m_registrations)
			{
				if (it.componentName == componentName)
					return it.componentAddress;
			}

			Console::Error("No Component named " + componentName + " exist in Component Registry");

			return nullptr;
		}

	private:
		struct Registration
		{
			std::string componentName;
			ComponentBase* componentAddress;
		};

		std::vector<Registration> m_registrations;
	};
}

#endif
#pragma once
#ifndef ENTITY_LisT_H
#define ENTITY_LisT_H

#include "entity.h"

namespace Chin
{
	class EntityList
	{
	public:
		EntityList()
		{
			m_entities.reserve(g_defaultReservedComponentSize);
		}

		~EntityList()
		{

		}

		Entity* AddEntity()
		{
			if(m_freeIDs.empty())
			{
				m_entities.emplace_back(m_entities.size() + 1);

				return &m_entities.back();
			}
			else
			{
				Entity& entity = m_entities[GetFreeID()];

				entity.AddComponent("Transform");

				return &entity;
			}
		}

		void DeleteEntity(size_t ID)
		{
			if(ID <= m_entities.size() && ID != 0)
			{
				Entity& entity = m_entities[ID - 1];

				entity.DeleteAllComponents();
				entity.SetName("Unnamed " + std::to_string(ID));

				// Possibly switch with
				//m_entities[ID - 1] = Entity(ID);
			}
		}

		Entity* GetEntity(size_t ID)
		{
			if(ID <= m_entities.size() && ID != 0)
			{
				return &m_entities[ID - 1];
			}

			return nullptr;
		}

		Entity* GetEntity(const std::string& entityName)
		{
			return nullptr;
		}

		Entity* GetEntities(const std::string& entityName)
		{
			return nullptr;
		}

		Entity* GetEntities(size_t tagName)
		{
			return nullptr;
		}
	private:
		std::vector<Entity> m_entities;
		std::queue<size_t> m_freeIDs;

		size_t GetFreeID()
		{
			size_t entityID = 0;
			if(!m_freeIDs.empty())
			{
				entityID = m_freeIDs.front();
				m_freeIDs.pop();
			}
			else
			{
				entityID = m_entities.size() + 1;
			}

			return entityID;
		}

		bool IsOutOfBounds(size_t ID)
		{
			return false;
		}
	};
}

#endif
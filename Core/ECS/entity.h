#pragma once
#ifndef ENTITY_H
#define ENTITY_H

#include "component_registry.h"
#include "prefab_registry.h"

namespace Chin
{
	struct TransformComponentData;

	class Entity
	{
	public:
		Entity(const size_t ID) : m_name("Unnamed " + std::to_string(ID))
		{
			AddComponent("Transform");
		}

		// Used to create a new entity from a prefab
		// Has to recreate data within components to be usable
		Entity(const Entity& source) = delete;

		Entity& operator=(const Entity& source) = delete;

		// Has to recreate data's parent within components to be usable
		Entity(Entity&& source) :
			m_name(std::move(source.m_name)),
			m_components(std::move(source.m_components))
		{
			source.m_components.clear();
		}

		Entity& operator=(Entity&& source) = delete;

		~Entity()
		{
			DeleteAllComponents();
		}

		void CopyFrom(const Entity& source)
		{
			m_name = source.m_name;

			for(auto pair : source.m_components)
			{
				ComponentBase* componentAddress = pair.first;

				try
				{
					// Replace with copy constructor
					m_components.push_back(std::pair<ComponentBase*, Handle>(componentAddress, componentAddress->CreateData(this)));
				}
				catch(std::exception& e)
				{
					Console::Exception(e);
					Console::Warning("Component " + componentAddress->GetName() + " added unsuccessfully");
				}
			}

			Console::Log("Copied succesfully");
		}

		void AddComponent(ComponentBase* componentAddress)
		{
			try
			{
				m_components.push_back(std::pair<ComponentBase*, Handle>(componentAddress, componentAddress->CreateData(this)));
			}
			catch(std::exception& e)
			{
				Console::Exception(e);
				Console::Warning("Component " + componentAddress->GetName() + " added unsuccessfully");
			}
		}

		// NEED TO HANDLE EXCEPTIONS
		void AddComponent(const std::string& componentName)
		{
			ComponentBase* componentAddress = RetrieveComponentAddress(componentName);

			if(!componentAddress)
			{
				Console::Error("Component " + componentName + " Is not defined in component registry");
				return;
			}

			if(RetrievePairIterator(componentAddress))
			{
				Console::Error("Component " + componentName + " already exist within entity");
				return;
			}

			AddComponent(componentAddress);
		}

		void DeleteComponent(const std::string& componentName)
		{
			if(auto pairIterator = RetrievePairIterator(RetrieveComponentAddress(componentName)))
			{
				const auto pair = pairIterator.value();

				ComponentBase* componentPtr = pair->first;
				const Handle componentHandle = pair->second;

				componentPtr->DestroyData(componentHandle);

				m_components.erase(pair);
			}
		}

		void DeleteAllComponents()
		{
			for(auto it = m_components.rbegin(); it != m_components.rend(); it++)
			{
				ComponentBase* componentPtr = it->first;
				const Handle componentHandle = it->second;

				componentPtr->DestroyData(componentHandle);
			}

			m_components.clear();
		}

		void UseComponentList(const PrefabRegistry::ComponentList& componentList)
		{
			DeleteAllComponents();

			m_components.reserve(componentList.size());

			for(auto it : componentList)
			{
				AddComponent(it);
			}
		}

		bool HasComponent(const std::string& componentName)
		{
			if (auto pairIterator = RetrievePairIterator(RetrieveComponentAddress(componentName)))
			{
				return true;
			}

			return false;
		}

		const std::vector<std::pair<ComponentBase*, Handle>>& GetComponents()
		{
			return m_components;
		}

		ComponentBase::Data* GetComponentData(const std::string& componentName)
		{
			auto pairIterator = RetrievePairIterator(RetrieveComponentAddress(componentName));

			if (!pairIterator)
			{
				return nullptr;
			}

			const auto pair = pairIterator.value();
			ComponentBase* componentPtr = pair->first;
			const Handle componentHandle = pair->second;

			return componentPtr->GetData(componentHandle);
		}

		template<typename T>
		T* GetComponentVariablePtr(const std::string& componentName, SID variableName)
		{
			auto pairIterator = RetrievePairIterator(RetrieveComponentAddress(componentName));

			if (!pairIterator)
			{
				return nullptr;
			}

			const auto pair = pairIterator.value();
			ComponentBase* componentPtr = pair->first;
			const Handle componentHandle = pair->second;

			return static_cast<T*>(componentPtr->RetrieveVariablePtr(componentHandle, varaiableName));
		}

		std::unique_ptr<DataFieldBase> GetDataField(const std::string& componentName, SID variableName)
		{
			auto pairIterator = RetrievePairIterator(RetrieveComponentAddress(componentName));

			if (!pairIterator)
			{
				return nullptr;
			}

			const auto pair = pairIterator.value();
			ComponentBase* componentPtr = pair->first;
			const Handle componentHandle = pair->second;

			return componentPtr->RetrieveDataField(componentHandle, variableName);
		}

		static ComponentRegistry& GetComponentRegistry() { static ComponentRegistry m_componentRegistry; return m_componentRegistry; }

		void SetName(const std::string& name)
		{
			m_name = name;
		}

		const std::string& GetName()
		{
			return m_name;
		}
	private:
		std::string m_name;

		std::vector<std::pair<ComponentBase*, Handle>> m_components;

		ComponentBase* RetrieveComponentAddress(const std::string& componentName)
		{
			return GetComponentRegistry().FindComponent(componentName);
		}

		std::optional<std::vector<std::pair<ComponentBase*, Handle>>::iterator> RetrievePairIterator(ComponentBase* componentAddress)
		{
			for(auto it = m_components.begin(); it != m_components.end(); it++)
			{
				if(it->first == componentAddress)
				{
					return it;
				}
			}

		#ifdef DEBUG
			if(componentAddress)
				Console::Log("Component " + componentAddress->GetName() + " does not exist in Entity");
		#endif

			return {};
		}
	};
}

#endif
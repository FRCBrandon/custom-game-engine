#include "component_base.h"
#include "entity.h"

namespace Chin
{
	bool ComponentBase::Data::CreateLink(SID inputName, const std::string & componentName, SID outputName)
	{
		// Does variable [inputName] exist?
		if(auto inputField = GetDataField(inputName))
		{
			// Is variable [inputName] an input variable?
			auto inputPtr = dynamic_cast<DataField<InputBase>*>(inputField.get());
			if(inputPtr)
			{
				// Does variable [outputName] exist and Is it an output variable?
				auto input = inputPtr->Get();
				if(auto outputPtr = RequestVariable<OutputBase>(componentName, outputName))
				{
					input->SetOutput(outputPtr);
					return true;
				}
				// Output Is invalid
				// NEED TO THROW
			}
			// [inputName] Is not an input variable
		}
		// Variable doesn't exist

		return false;
	}

	ComponentBase::Data* ComponentBase::Data::RequestComponent(const std::string& componentName)
	{
		Entity* const parent = GetParent();
		Data* componentData = parent->GetComponentData(componentName);
		return componentData;
	}

	ComponentBase::Data* ComponentBase::Data::RequireComponent(const std::string& componentName)
	{
		if (auto componentData = RequestComponent(componentName))
		{
			return componentData;
		}

		throw UnavailableComponentException();
	}
}
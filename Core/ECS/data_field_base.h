#pragma once
#ifndef DATA_FIELD_BASE_H
#define DATA_FIELD_BASE_H

namespace Chin
{
	struct DataFieldBase
	{
		virtual ~DataFieldBase() {}
	};
}

#endif
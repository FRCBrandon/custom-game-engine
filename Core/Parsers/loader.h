#pragma once
#ifndef LOADER_H
#define LOADER_H

#include "version.h"
#include <fstream>
#include "Core/core.h"
#include "Utility/console.h"

std::string readFile(const std::string& filepath);

std::string retrieveVersion();

#endif // LOADER_H

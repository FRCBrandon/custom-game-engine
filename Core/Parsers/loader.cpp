#include "loader.h"

std::string readFile(const std::string& filepath)
{
    std::string line = "";
    std::string fin = "";
    std::ifstream file(filepath);
    if(file.is_open())
    {
        while(std::getline(file, line))
        {
            fin.append(line + "\n");
        }
        file.close();
    }
    return fin;
}

std::string retrieveVersion()
{
    std::string version;
    version.append(AutoVersion::STATUS_SHORT).append(AutoVersion::FULLVERSION_STRING);

    return version;
}

#pragma once
#ifndef CORE_H
#define CORE_H

#include "Modules/Physics/precision.h"
#include "Utility/console.h"

#include <cassert>
#include <variant>
#include <functional>
#include <string>

#define ASSERT(expression) assert(expression)
#define STATIC_ASSERT(expression) static_assert(expression)

namespace Chin
{
	using Variant = std::variant<int, float, void*, std::string>;

	using SID = std::string;

	extern std::hash<std::string> g_stringHasher;

	const SID Hash_String(const std::string& string);

	constexpr size_t g_defaultAmountOfEntities = 1024;
	constexpr size_t g_defaultReservedComponentSize = 1024;
	STATIC_ASSERT(g_defaultAmountOfEntities >= g_defaultReservedComponentSize);
}

#endif
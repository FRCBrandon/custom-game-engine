#pragma once
#ifndef MESSAGE_H
#define MESSAGE_H

#include "Core/core.h"

namespace Chin
{
	struct Message
	{
		std::string m_key;
		Variant m_data;
	};
}

#endif
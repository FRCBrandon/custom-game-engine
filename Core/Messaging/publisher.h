#pragma once
#ifndef PUBLisHER_H
#define PUBLisHER_H

#include "messaging_poll.h"
#include <vector>

namespace Chin
{
	struct Subscriber;

	class Publisher
	{
	public:
		void Subscribe(Subscriber* sub)
		{
			m_subscribers.push_back(sub);
		}

		void Unsubscribe(Subscriber* sub)
		{
			for (auto it = m_subscribers.begin(); it != m_subscribers.end(); it++)
			{
				if ((*it) == sub)
				{
					m_subscribers.erase(it);
					return;
				}
			}
		}

		void SendMessage(Message msg)
		{
			for (auto it : m_subscribers)
			{
				it->HandleMessage(msg);
			}
		}

		void SendMessage(Message msg, Frames duration)
		{
			for (auto it : m_subscribers)
			{
				m_msgPoll.AddPackage(msg, duration, it);
			}
		}

		void SendMessage(Message msg, Seconds duration)
		{
			for (auto it : m_subscribers)
			{
				m_msgPoll.AddPackage(msg, duration, it);
			}
		}
	private:
		std::vector<Subscriber*> m_subscribers;
	};
}

#endif
#pragma once
#ifndef MESSAGING_POLL_H
#define MESSAGING_POLL_H

#include "subscriber.h"

namespace Chin
{
	struct Subscriber;

	// Needed by Publishers
	class MessagingPoll
	{
	private:
		using Frames = uint32_t;
		using Seconds = float;
		using Timer = std::variant<Frames, Seconds>;
	public:
		void StepAll(real deltaTime)
		{
			for (auto it = m_packages.begin(); it != m_packages.end();)
			{
				Timer& timer = (*it).m_duration;
				bool isTimerSpent = false;

				if (Frames& framesLeft = *std::get_if<Frames>(&timer))
				{
					framesLeft -= 1;

					if (framesLeft == 0)
						isTimerSpent = true;
				}
				else
				{
					float& secondsLeft = *std::get_if<Seconds>(&timer);
					secondsLeft -= deltaTime;

					if (secondsLeft <= 0.0f)
						isTimerSpent = true;
				}

				if (isTimerSpent == true)
				{
					it->m_destination->HandleMessage(it->m_msg);
					m_packages.erase(it);
				}
				else
					it++;
			}
		}

		void AddPackage(Message msg, Frames duration, Subscriber* destination)
		{
			m_packages.push_back(Package{ msg, duration, destination });
		}

		void AddPackage(Message msg, Seconds duration, Subscriber* destination)
		{
			m_packages.push_back(Package{ msg, duration, destination });
		}
	private:
		struct Package
		{
			Message m_msg;
			Timer m_duration;
			Subscriber* m_destination;

			// Not Needed
			Package(Message msg, Frames duration, Subscriber* destination) : m_msg(msg), m_destination(destination) { m_duration = duration; }
			// Not Needed
			Package(Message msg, Seconds duration, Subscriber* destination) : m_msg(msg), m_destination(destination) { m_duration = duration; }
		};

		std::vector<Package> m_packages;
	};
}

#endif
#pragma once
#ifndef PUBLisHER_BASE_H
#define PUBLisHER_BASE_H

#include "messaging_poll.h"

namespace Chin
{
	struct Subscriber;

	class PublisherBase
	{
	public:
		virtual void Subscribe(Subscriber* sub) = 0;
		virtual void Unsubscribe(Subscriber* sub) = 0;
	private:
		static MessagingPoll m_msgPoll;
	};
}

#endif

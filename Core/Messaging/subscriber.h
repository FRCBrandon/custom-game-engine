#pragma once
#ifndef SUBSCRIBER_H
#define SUBSCRIBER_H

#include "message.h"

namespace Chin
{
	class Publisher;

	struct Subscriber
	{
	public:
		virtual void HandleMessage(Message msg) = 0;

		void SubscribeTo(Publisher* pub)
		{
			pub->Subscribe(this);
		}

		void UnsubscribeTo(Publisher* pub)
		{
			pub->Unsubscribe(this);
		}
	};
}

#endif
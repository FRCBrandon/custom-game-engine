#pragma once
#ifndef CONSOLE_H
#define CONSOLE_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <optional>
#include "exception.h"

#define DEBUG 1
#define CONSOLE_VERBOSE_LOGGING 0

namespace Chin
{
	namespace Console
	{
		enum class Type { User, Info, Error, Warning };

		// Set logging function type to output to a specific file
		void SetOStream(Type type, const char *filePath);

		// Set logging function type to output to a specific stream buffer
		void SetOStream(Type type, std::streambuf* buf);

		// Adds tab character after string
		std::string Indent(std::string string);

		// If true then log timestamps in command/file logs
		static bool logTime = false;

		// Returns string of system's current time
		std::string currentTime();

		// Format system's current time to be outputted in logging functions
		std::optional<std::string> formatCurrentTime();

		// For outputting any type of logging function
		void Output(Type type, std::string string);

		// For outputting formatted string to user
		void User(std::string string);

		// For logging function calls, constructors, and destructors
		void Log(std::string string);
		void luaLog(std::string string);

		// For outputting errors to command line or file
		void Error(std::string string);
		void luaError(std::string string);

		// For outputting warnings to same place as Error()
		void Warning(std::string string);
		void luaWarning(std::string string);

		void Exception(std::exception exception);

		class LogFileStream : public std::ofstream
		{
		public:
			template <typename T>
			void operator<<(const T& data) { std::ofstream::operator<<(data); std::ofstream::flush(); }

			static LogFileStream& Get() { static LogFileStream ls; return ls; }

			void Open(const char* filePath) { open(filePath); }

		private:
			LogFileStream() {}
			LogFileStream(const LogFileStream&) = delete;
			void operator=(const LogFileStream&) = delete;
		};

		class ErrorFileStream : public std::ofstream
		{
		public:
			template <typename T>
			void operator<<(const T& data) { std::ofstream::operator<<(data); std::ofstream::flush(); }

			static ErrorFileStream& Get() { static ErrorFileStream ls; return ls; }

			void Open(const char* filePath) { open(filePath); }

		private:
			ErrorFileStream() {}
			ErrorFileStream(const ErrorFileStream&) = delete;
			void operator=(const ErrorFileStream&) = delete;
		};
	}
}

#endif // CONSOLE_H
#include "Utility/console.h"

namespace Chin
{
	namespace Console
	{
		void SetOStream(Type type, const char *filePath)
		{
			switch (type)
			{
			case Type::Info:
				LogFileStream::Get().Open(filePath);
				std::clog.rdbuf(LogFileStream::Get().rdbuf());
				break;
			case Type::Error:
				ErrorFileStream::Get().Open(filePath);
				std::cerr.rdbuf(ErrorFileStream::Get().rdbuf());
				break;
			}
		}

		void SetOStream(Type type, std::streambuf* buf)
		{
			switch (type)
			{
			case Type::Info:
				std::clog.rdbuf(buf);
				break;
			case Type::Error:
				std::cerr.rdbuf(buf);
				break;
			}
		}

		std::string Indent(std::string string)
		{
			return std::string(string + "\t");
		}

		std::string currentTime()
		{
			auto currentTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
			struct tm time;
			localtime_s(&time, &currentTime);
			std::stringstream timestring;
			timestring << time.tm_hour << ":" << time.tm_min << ":";
			if (time.tm_sec < 10)
				timestring << 0;
			timestring << time.tm_sec;

			return timestring.str();
		}

		std::optional<std::string> formatCurrentTime()
		{
			if (logTime)
				return Indent(currentTime());
			else
				return {};
		}

		void Output(Type type, std::string string)
		{
			switch (type)
			{
			case Type::User:
				User(string);
				break;
			case Type::Info:
				Log(string);
				break;
			case Type::Error:
				Error(string);
				break;
			case Type::Warning:
				Warning(string);
				break;
			}
		}

		void User(std::string string)
		{
			std::cout << string << std::endl;
		}

		void Log(std::string string)
		{
			std::clog << Indent("Info:") << formatCurrentTime().value_or("") << string << std::endl;
		}

		void luaLog(std::string string)
		{
			std::clog << Indent("Lua Info:") << formatCurrentTime().value_or("") << string << std::endl;
		}

		void Error(std::string string)
		{
			std::cerr << Indent("Error:") << formatCurrentTime().value_or("") << string << std::endl;
		}

		void luaError(std::string string)
		{
			std::cerr << Indent("Lua Error:") << formatCurrentTime().value_or("") << string << std::endl;
		}

		void Warning(std::string string)
		{
			std::cerr << Indent("Warning:") << formatCurrentTime().value_or("") << string << std::endl;
		}

		void luaWarning(std::string string)
		{
			std::cerr << Indent("Lua Warning:") << formatCurrentTime().value_or("") << string << std::endl;
		}

		void Exception(std::exception exception)
		{
			std::cerr << Indent("Exception caught:") << formatCurrentTime().value_or("") << exception.what() << std::endl;
		}
	}
}
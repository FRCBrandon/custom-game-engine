#pragma once
#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <iostream>
#include <exception>

namespace Chin
{
	struct Exception : public std::exception
	{
		virtual const char* what() const throw()
		{
			return "Chin Exception";
		}
	};
}

#endif
ParticlePhysicsComponent =
{
	position = vector3(0.0, 0.0, 0.0),

	velocity = vector3(0.0, 0.0, 0.0),

	acceleration = vector3(0.0, 0.0, 0.0),

	inverseMass = 0.0,

	damping = 0.0,

	handleMessage = function (msg)

		if(msg.key == "position")
			position = msg.data
		if(msg.key == "velocity")
			velocity = msg.data
		if(msg.key == "acceleration")
			acceleration = msg.data
		if(msg.key == "inverseMass")
			inverseMass = msg.data
		if(msg.key == "damping")
			damping = msg.data

	end,

	Fn = function (time)

		assert(duration > 0.0)
		if(inverseMass <= 0.0) return

		position.addScaledVector(velocity, duration)

		vector3 resultingAcc = acceleration

		velocity.addScaledVector(resultingAcc, duration)

		velocity = velocity * real_pow(damping, duration)

		sendMessage(Message("position", position))

	end
}

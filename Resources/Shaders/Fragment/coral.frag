#version 330 core

in vec3 Position;

uniform vec3 objectColor;
uniform vec3 lightColor;

out vec4 frag_color;

void main()
{
    frag_color = vec4(objectColor * lightColor, 1.0f);
    // frag_color = vec4(Position * lightColor, 1.0);
}

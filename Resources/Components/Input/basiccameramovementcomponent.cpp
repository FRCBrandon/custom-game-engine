#include "stdafx.h"
#include "basiccameramovementcomponent.h"
#include "Modules/Core/Classes/Entities/entity.h"

namespace Chin
{
	void BasicCameraMovementComponent::onBind()
	{
		if (m_entityPtr->hasComponentByName("Camera"))
		{
			auto camComponentPtr = static_cast<CameraComponent*>(m_entityPtr->getComponentByName("Camera"));
			m_camera = camComponentPtr->m_camera.get();

			registerSubscriber(camComponentPtr);
		}
		else
		{
			Console::Error( "No Camera Component for " + m_name );
		}
	}

	void BasicCameraMovementComponent::updateInput(real dt)
	{
		auto input = InputHandler::Get();

		auto camera = *m_camera;

		if (input->checkKey(sf::Keyboard::W))
			camera.Translate(Camera::Direction::FORWARD, dt);
		if (input->checkKey(sf::Keyboard::A))
			camera.Translate(Camera::Direction::LEFT, dt);
		if (input->checkKey(sf::Keyboard::S))
			camera.Translate(Camera::Direction::BACK, dt);
		if (input->checkKey(sf::Keyboard::D))
			camera.Translate(Camera::Direction::RIGHT, dt);

		if (auto delta = input->deltaScroll)
		{
			camera.Zoom(delta);
			sendMessage(Message("fov", camera.getFOV()));
		}

		auto delta = input->deltaMousePosition;
		if (delta != sf::Vector2i(0, 0))
		{
			camera.Look(delta.x, -delta.y);

			sendMessage(Message("rotation", camera.getRotationVector3()));
			sendMessage(Message("cameraFront", vector3(camera.getFront())));
		}

		sendMessage(Message("position", vector3(camera.getPosition())));
	}
}
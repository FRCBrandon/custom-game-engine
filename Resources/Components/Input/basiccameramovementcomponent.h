#pragma once
#ifndef BASICCAMERAMOVEMENTCOMPONENT_H
#define BASICCAMERAMOVEMENTCOMPONENT_H

#include "Modules/Core/Classes/Components/inputcomponent.h"
#include "Modules/Input/Classes/Systems/inputhandler.h"
#include "Modules/Graphics/Classes/Systems/openglrendersystem.h"
#include "Resources/Components/Graphics/cameracomponent.h"

namespace Chin
{
	// Component that controls Camera's movement in CameraComponent
	class BasicCameraMovementComponent : public InputComponent
	{
	public:
		Camera *m_camera;
	public:
		BasicCameraMovementComponent(Entity* parent) : InputComponent(parent, "Basic Camera Movement") {}
		BasicCameraMovementComponent() : InputComponent(sizeof(Data), RESERVED_ENTITY_SIZE, "Basic Camera Movement") {}

		std::unique_ptr<Component> Clone() override { return std::make_unique<BasicCameraMovementComponent>(*this); }

		void onBind() override;

		void updateInput(real dt) override;
	};
}
#endif // BASICCAMERAMOVEMENTCOMPONENT_H

#include "Resources/Components/Physics/particlephysicscomponent.h"
#include "Modules/Core/Classes/Entities/entity.h"

namespace Chin
{
	void ParticlePhysicsComponent::onCreate(Component::Data* data)
	{
		Subscribe(ptr->this, data->getEntityPtr()->getComponentByName("Transform"));
	}

	void ParticlePhysicsComponent::handleMessage(Message msg, Component::Data* data)
	{
		bool match = true;
		auto& key = msg.getKey();
		auto& value = msg.getVariant();
		auto ptr = static_cast<Data*>(data);

		if (key == "velocity")
			ptr->setVelocity(VECTOR3(value));
		else if (key == "acceleration")
			ptr->setAcceleration(VECTOR3(value));
		else if (key == "mass")
			ptr->setMass(REAL(value));
		else if (key == "inverseMass")
			ptr->setInverseMass(REAL(value));
		else if (key == "damping")
			ptr->setDamping(REAL(value));
		else
			match = false;

		if (match)
			ptr->sendMessage(msg);
	}

	Variant ParticlePhysicsComponent::getVariable(std::string key)
	{
		if (key == "velocity")
			return getVelocity();
		else if (key == "acceleration")
			return getAcceleration();
		else if (key == "mass")
			return getMass();
		else if (key == "inverseMass")
			return getInverseMass();
		else if (key == "damping")
			return getDamping();

		return Variant();
	}

	void ParticlePhysicsComponent::Integrate(real dt, Component::Data* data)
	{
	    ASSERT(dt > 0.0f);

		auto ptr = static_cast<Data*>(data);

	    if(ptr->m_inverseMass <= 0.0f) return;

		ptr->sendMessage(Message("Translate", ptr->m_velocity * dt));

	    vector3 resultingAcc = ptr->m_acceleration;

		ptr->m_velocity.addScaledVector(resultingAcc, dt);

		ptr->m_velocity *= real_pow(ptr->m_damping, dt);
	}

	void ParticlePhysicsComponent::Data::setMass(const real mass)
	{
	    ASSERT(mass != 0);
	    m_inverseMass = ((real)1.0)/mass;
	}

	void ParticlePhysicsComponent::Data::setInverseMass(const real mass)
	{
	    m_inverseMass = mass;
	}

	void ParticlePhysicsComponent::Data::setVelocity(vector3 vector)
	{
		m_velocity = vector;
	}

	void ParticlePhysicsComponent::Data::setAcceleration(vector3 vector)
	{
		m_acceleration = vector;
	}

	void ParticlePhysicsComponent::Data::setDamping(real f)
	{
		m_damping = f;
	}
}
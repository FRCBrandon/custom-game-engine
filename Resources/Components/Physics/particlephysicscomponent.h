#pragma once
#ifndef PARTICLEPHYSICSCOMPONENT_H
#define PARTICLEPHYSICSCOMPONENT_H

#include "Modules/Physics/Classes/Data/vector3.h"
#include "Modules/Core/Classes/Components/physicscomponent.h"

namespace Chin
{
	// Component that computes position with Particle Physics
	class ParticlePhysicsComponent : public PhysicsComponent
	{
	public:
		struct Data : public Component::Data
		{
			vector3 m_velocity, m_acceleration;
			real m_inverseMass, m_damping;

			void setMass(const real mass);
			real getMass() { return 1 / m_inverseMass; }

			void setInverseMass(const real mass);
			real getInverseMass() { return m_inverseMass; }

			void setVelocity(vector3 vector);
			vector3 getVelocity() { return m_velocity; }

			void setAcceleration(vector3 vector);
			vector3 getAcceleration() { return m_acceleration; }

			void setDamping(real f);
			real getDamping() { return m_damping; }
		};
	public:
		ParticlePhysicsComponent() : PhysicsComponent(sizeof(Data), RESERVED_ENTITY_SIZE, "Particle Physics") {};

		void onCreate(Component::Data* data) override;

		void handleMessage(Message msg, Component::Data* data) override;

		Variant getVariable(std::string variableName) override;

		virtual void Fn(Data data, real duration) { Console::Error("No Fn(time) function in " + toString()); }

	    void Integrate(real dt, Component::Data* data) override;
	};
}
#endif // PARTICLEPHYSICSCOMPONENT_H

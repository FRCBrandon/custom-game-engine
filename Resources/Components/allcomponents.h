#pragma once
#ifndef ALLCOMPONENTS_H
#define ALLCOMPONENTS_H

#include "Resources/Components/Data/transformcomponent.h"
#include "Resources/Components/Physics/particlephysicscomponent.h"
#include "Resources/Components/Graphics/meshcomponent.h"
#include "Resources/Components/Graphics/meshrenderercomponent.h"
#include "Resources/Components/Graphics/cameracomponent.h"
#include "Resources/Components/Input/basiccameramovementcomponent.h"
#include "Resources/Components/Graphics/lightcomponent.h"

#endif // ALLCOMPONENTS_H

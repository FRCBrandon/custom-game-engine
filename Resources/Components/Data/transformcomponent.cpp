#include "Resources/Components/Data/transformcomponent.h"
#include "Modules/Core/Classes/Entities/entity.h"

namespace Chin
{
	void TransformComponent::handleMessage(Message msg, Component::Data* data)
	{
		auto ptr = static_cast<TransformComponent::Data*>(data);

		bool match = true;
		auto& key = msg.getKey();
		auto& value = msg.getVariant();

		if(key == "Translate")
			ptr->m_transform.Translate(VECTOR3(value));
		else if (key == "Scale")
			ptr->m_transform.Scale(REAL(value));
		else if (key == "Rotate")
			ptr->m_transform.Rotate(VECTOR3(value));
		else if (key == "position")
			ptr->m_transform.setPosition(VECTOR3(value));
		else if (key == "scale")
			ptr->m_transform.setScale(VECTOR3(value));
		else if (key == "rotation")
			ptr->m_transform.setRotation(VECTOR3(value));
		else if (key == "transform")
			ptr->m_transform = *static_cast<Transform*>(VOID_PTR(value));
		else
			match = false;

		//if (match)
			//sendMessage(msg);
	}

	Variant TransformComponent::getVariable(std::string variableName)
	{
		/*
		if (variableName == "position")
			return m_transform.getPosition();
		else if (variableName == "scale")
			return m_transform.getScale();
		else if (variableName == "rotation")
			return m_transform.getRotation();
		else if (variableName == "transform")
			return &m_transform;
		*/
		return Variant();
	}
}

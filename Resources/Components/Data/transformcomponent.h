#pragma once
#ifndef TRANSFORMCOMPONENT_H
#define TRANSFORMCOMPONENT_H

#include "Modules/Core/Classes/Components/datacomponent.h"
#include "Modules/Core/Structs/Data/transform.h"

namespace Chin
{
	class TransformComponent : public DataComponent
	{
	public:
		struct Data : public Component::Data
		{
			Transform m_transform;
		};

		TransformComponent() : DataComponent(sizeof(Data), RESERVED_ENTITY_SIZE, "Transform") {}

		void handleMessage(Message msg, Component::Data* data) override;

		Variant getVariable(std::string variableName) override;
	};
}
#endif
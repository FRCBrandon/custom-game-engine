#pragma once
#ifndef MESHCOMPONENT_H
#define MESHCOMPONENT_H

#include "Modules/Graphics/Classes/Meshes/mesh.h"
#include "Modules/Core/Classes/Components/datacomponent.h"

namespace Chin
{
	// Component that holds data of a Mesh
	class MeshComponent : public DataComponent
	{
	public:
		std::unique_ptr<Mesh> m_mesh;

		MeshComponent(Entity* parent) : DataComponent(parent, "Mesh") {}
		MeshComponent() : DataComponent(sizeof(Data), RESERVED_ENTITY_SIZE, "Mesh") {}

		std::unique_ptr<Component> Clone() override
		{
			auto ptr = std::make_unique<MeshComponent>();
			if (m_mesh)
				ptr->m_mesh.reset(new Mesh(*m_mesh));
			return ptr;
		}

		void handleMessage(Message msg) override;

		Variant getVariable(std::string variableName) override;
	};
}
#endif // !MESHCOMPONENT_H

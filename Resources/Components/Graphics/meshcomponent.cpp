#include "stdafx.h"
#include "meshcomponent.h"

namespace Chin
{
	void MeshComponent::handleMessage(Message msg)
	{
		if (msg.getKey() == "mesh")
		{
			m_mesh.reset(static_cast<Mesh*>(std::get<void*>(msg.getVariant())));
			sendMessage(msg);
		}
	}

	Variant MeshComponent::getVariable(std::string variableName)
	{
		if (variableName == "mesh")
			return m_mesh.get();

		return Variant();
	}
}

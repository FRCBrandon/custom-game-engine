#include "stdafx.h"
#include "lightcomponent.h"
#include "Modules/Core/Classes/Entities/entity.h"

namespace Chin
{
	std::unique_ptr<Component> LightComponent::Clone()
	{
		auto ptr = std::make_unique<LightComponent>();
		if (m_light)
		{
			switch (m_light->m_type)
			{
			case Light::Type::Directional:
				ptr->m_light = std::make_unique<DirectionalLight>(*static_cast<DirectionalLight*>(m_light.get()));
				break;
			case Light::Type::Point:
				ptr->m_light = std::make_unique<PointLight>(*static_cast<PointLight*>(m_light.get()));
				break;
			case Light::Type::Spot:
				ptr->m_light = std::make_unique<Chin::SpotLight>(*static_cast<SpotLight*>(m_light.get()));
				break;
			}
		}
		return ptr;
	}

	void LightComponent::onBind()
	{
		m_entityPtr->getComponentByName("Transform")->registerSubscriber(this);
	}

	void LightComponent::handleMessage(Message msg)
	{
		bool match = true;
		auto& key = msg.getKey();
		auto& value = msg.getVariant();

		auto renderer = OpenGLRenderSystem::Get();

		if (key == "light")
		{
			m_light.reset(static_cast<Light*>(std::get<void*>(value)));
			switch (m_light->m_type)
			{
			case Light::Type::Directional:
				renderer->setDirectionalLight(static_cast<DirectionalLight*>(m_light.get()));
				break;
			case Light::Type::Point:
				renderer->setPointLight(static_cast<PointLight*>(m_light.get()));
				break;
			case Light::Type::Spot:
				renderer->setSpotLight(static_cast<SpotLight*>(m_light.get()));
				break;
			}
		}
		else
		{
			if (key == "type")
			{
				switch (LIGHT_TYPE(ENUM_TYPE(value)))
				{
				case Light::Type::Directional:
					m_light.reset(new DirectionalLight());
					renderer->setDirectionalLight(static_cast<DirectionalLight*>(m_light.get()));
					break;
				case Light::Type::Point:
					m_light.reset(new PointLight());
					renderer->setPointLight(static_cast<PointLight*>(m_light.get()));
					break;
				case Light::Type::Spot:
					m_light.reset(new SpotLight());
					renderer->setSpotLight(static_cast<SpotLight*>(m_light.get()));
					break;
				}
			}
			else if (key == "ambient")
				m_light->m_ambient = GLM_VEC3(value);
			else if (key == "diffuse")
				m_light->m_diffuse = GLM_VEC3(value);
			else if (key == "specular")
				m_light->m_specular = GLM_VEC3(value);
			else
			{
				if (m_light->m_type != Light::Type::Directional)
				{
					auto lightPtr = static_cast<AttenuatedLight*>(m_light.get());
					if (key == "linear")
						lightPtr->m_linear = std::get<float>(value);
					else if (key == "quadratic")
						lightPtr->m_quadratic = std::get<float>(value);
					else if (key == "constant")
						lightPtr->m_constant = std::get<float>(value);

					if (m_light->m_type == Light::Type::Point)
					{
						auto lightPtr = static_cast<PointLight*>(m_light.get());
						if (key == "position")
							lightPtr->m_position = Toglm(VECTOR3(value));
					}
					else
					{
						auto lightPtr = static_cast<SpotLight*>(m_light.get());
						if (key == "position")
							lightPtr->m_position = Toglm(VECTOR3(value));
						else if (key == "direction" || key == "rotation")
							lightPtr->m_direction = Toglm(VECTOR3(value));
						else
							match = false;
					}
				}
				else if (key == "direction" || key == "rotation")
				{
					auto lightPtr = static_cast<DirectionalLight*>(m_light.get());
					lightPtr->m_direction = Toglm(VECTOR3(value));
				}
				else
					match = false;
			}
			if (match)
				sendMessage(msg);
		}
	}

	void LightComponent::Render()
	{
		auto renderer = OpenGLRenderSystem::Get();

		if (m_light->m_type == Light::Type::Directional)
		{
			auto lightPtr = static_cast<DirectionalLight*>(m_light.get());

			lightPtr->m_direction = Toglm(m_entityPtr->getTransform()->getRotation());
		}
		else if (m_light->m_type == Light::Type::Point)
		{
			auto lightPtr = static_cast<PointLight*>(m_light.get());

			lightPtr->m_position = Toglm(m_entityPtr->getTransform()->getPosition());
		}
		else if (m_light->m_type == Light::Type::Spot)
		{
			auto lightPtr = static_cast<SpotLight*>(m_light.get());

			lightPtr->m_position = Toglm(m_entityPtr->getTransform()->getPosition());
			auto g = GLM_VEC3(m_entityPtr->getComponentByName("Camera")->getVariable("cameraFront"));
			lightPtr->m_direction = g;
		}
	}

	Variant LightComponent::getVariable(std::string variableName)
	{
		if (variableName == "light")
			return m_light.get();
		return Variant();
	}
}

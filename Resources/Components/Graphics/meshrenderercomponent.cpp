#include "Resources/Components/Graphics/meshrenderercomponent.h"
#include "Modules/Core/Classes/Entities/entity.h"

namespace Chin
{
	void MeshRendererComponent::onCreate(Component::Data* data)
	{
		if (auto meshptr = static_cast<MeshComponent*>(requireComponent("Mesh")))
		{
			m_mesh = static_cast<Mesh*>(std::get<void*>(meshptr->getVariable("mesh")));
			meshptr->registerSubscriber(this);
		}
		else
		{
			Console::Error( "Error in MeshRendererComponent" );
		}
	}

	void MeshRendererComponent::onDestroy(Component::Data* data)
	{
	}

	void MeshRendererComponent::handleMessage(Message msg, Component::Data* data)
	{
		m_mesh = static_cast<Mesh*>(std::get<void*>(msg.getVariant()));
	}

	void MeshRendererComponent::Render(real dt, Component::Data* data)
	{
		if (m_mesh)
		{
			glm::mat4 model;
			Transform transform = *m_entityPtr->getTransform();

			model = glm::translate(model, Toglm(transform.getPosition()));
			model = glm::scale(model, Toglm(transform.getScale()));
			//model = glm::rotate(model, Toglm(transform.getRotation()));

			OpenGLRenderSystem::Get()->shaderprogrammes["standard"]->setMat4("model", model);

			m_mesh->Render();
		}
		else
			Console::Error( "Render failed, No Mesh Component for " + m_name );
	}
}
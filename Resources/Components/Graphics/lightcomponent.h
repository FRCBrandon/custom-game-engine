#pragma once
#ifndef LIGHTCOMPONENT_H
#define LIGHTCOMPONENT_H

#include "Modules/Core/Classes/Components/graphicscomponent.h"
#include "Modules/Graphics/Classes/Systems/openglrendersystem.h"
#include "Modules/Graphics/Classes/Abstractions/light.h"

namespace Chin
{
	class LightComponent : public GraphicsComponent
	{
	public:
		LightComponent() : GraphicsComponent(sizeof(Data), RESERVED_ENTITY_SIZE, "Light") {}

		std::unique_ptr<Component> Clone();

		std::unique_ptr<Light> m_light;

		void onBind() override;

		void handleMessage(Message msg) override;

		void Render() override;

		Variant getVariable(std::string variableName) override;
	};
}
#endif // LIGHTCOMPONENT_H

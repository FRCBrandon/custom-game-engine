#pragma once
#ifndef MESHRENDERERCOMPONENT_H
#define MESHRENDERERCOMPONENT_H

#include "Resources/Components/Graphics/meshcomponent.h"
#include "Modules/Graphics/Classes/Systems/openglrendersystem.h"

namespace Chin
{
	// Component that renders MeshComponent and interfaces between Mesh and RenderSystem
	class MeshRendererComponent : public GraphicsComponent
	{
	public:
		struct Data : Component::Data
		{
			Mesh* m_mesh;
		};
	public:
		MeshRendererComponent() : GraphicsComponent(sizeof(Data), RESERVED_ENTITY_SIZE, "Mesh Renderer") {}

		void onCreate(Component::Data* data) override;
		void onDestroy(Component::Data* data) override;

		void handleMessage(Message msg, Component::Data* data) override;

		void Render(real dt, Component::Data* data) override;
	};
}
#endif // !MESHRENDERERCOMPONENT_H

#pragma once
#ifndef CAMERACOMPONENT_H
#define CAMERACOMPONENT_H

#include "Modules/Core/Classes/Components/graphicscomponent.h"
#include "Modules/Graphics/Classes/Systems/openglrendersystem.h"

namespace Chin
{
	// Component that interfaces between the Camera and RenderSystem
	class CameraComponent : public GraphicsComponent
	{
	public:
		CameraComponent(Entity* parent) : GraphicsComponent(parent, "Camera") {}
		CameraComponent() : GraphicsComponent(sizeof(Data), RESERVED_ENTITY_SIZE, "Camera") {}

		std::unique_ptr<Component> Clone() override
		{
			auto ptr = std::make_unique<CameraComponent>();
			if (m_camera)
				ptr->m_camera.reset(new Camera(*m_camera));
			return ptr;
		}

		std::unique_ptr<Camera> m_camera;

		void onBind() override;

		void Render() override;

		void handleMessage(Message msg) override;

		Variant getVariable(std::string variableName) override;
	};

}
#endif // CAMERACOMPONENT_H

#include "cameracomponent.h"
#include "Modules/Core/Classes/Entities/entity.h"

namespace Chin
{
	void CameraComponent::onBind()
	{
		m_camera = std::make_unique<Camera>();
		OpenGLRenderSystem::Get()->setCamera(m_camera.get());

		registerSubscriber(m_entityPtr->getComponentByName("Transform"));
	}

	void CameraComponent::Render()
	{

	}

	void CameraComponent::handleMessage(Message msg)
	{
		bool match = true;
		auto& key = msg.getKey();
		auto& value = msg.getVariant();

		if (key == "position")
			m_camera->setPosition(Toglm(VECTOR3(value)));
		else if (key == "rotation")
			m_camera->setRotation(Toglm(VECTOR3(value)));
		else if (key == "set")
			OpenGLRenderSystem::Get()->setCamera(m_camera.get());
		else if (key == "camera")
		{
			m_camera.reset(static_cast<Camera*>(VOID_PTR(value)));
			OpenGLRenderSystem::Get()->setCamera(m_camera.get());
		}
		else if (key == "fov")
			m_camera->setFOVRadians(REAL(value));
		else if (key == "cameraPosition")
			m_camera->setPosition(GLM_VEC3(value));
		else if (key == "cameraRotation")
			m_camera->setRotation(GLM_VEC3(value));
		else if (key == "locked")
			m_camera->lockProperty(CAMERA_LOCK(ENUM_TYPE(value)));
		else if (key == "speed")
			m_camera->setSpeed(std::get<float>(value));
		else if (key == "sensitivity")
			m_camera->setSensitivity(std::get<float>(value));
		else if (key == "pitch")
			m_camera->setPitch(std::get<float>(value));
		else if (key == "roll")
			m_camera->setRoll(std::get<float>(value));
		else if (key == "yaw")
			m_camera->setYaw(std::get<float>(value));
		else if (key == "cameraFront")
			m_camera->setFront(Toglm(VECTOR3(value)));
		else if (key == "cameraUp")
			m_camera->setUp(Toglm(VECTOR3(value)));
		else
			match = false;

		if(match)
			sendMessage(msg);
	}

	Variant CameraComponent::getVariable(std::string variableName)
	{
		if (variableName == "camera")
			return Variant(m_camera.get());
		else if (variableName == "fov")
			return Variant(m_camera->getFOVRadians());
		else if (variableName == "cameraPosition")
			return Variant(m_camera->getPosition());
		else if (variableName == "speed")
			return Variant(m_camera->getSpeed());
		else if (variableName == "sensitivity")
			return Variant(m_camera->getSensitivity());
		else if (variableName == "pitch")
			return Variant(m_camera->getPitch());
		else if (variableName == "roll")
			return Variant(m_camera->getRoll());
		else if (variableName == "yaw")
			return Variant(m_camera->getYaw());
		else if (variableName == "cameraFront")
			return Variant(m_camera->getFront());
		else if (variableName == "cameraUp")
			return Variant(m_camera->getUp());
		return Variant();
	}
}

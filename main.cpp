#include "stdafx.h"
#include "Core/game_engine.h"

/*
Default Settings
depth bits = 32
stencil bits = 0
anti-aliasing = 0
version = 4.5
*/

// Sets Settings for creating window
sf::ContextSettings defaultContextSettings();

int main()
{
	Chin::GameEngine::Settings Settings
	{
		Chin::SFMLWindowSystem::Settings
		{
		800,
		600,
		"Title",
		sf::Style::Default,
		defaultContextSettings()
		}
	};

	Chin::GameEngine game(Settings);

	game.Initialize();
	game.Loop();
	game.Shutdown();

	//std::cout << sizeof(Chin::DynamicContiguousAllocator) << std::endl; // 52
	//std::cout << sizeof(Chin::ParticlePhysicsComponent) << std::endl; // 140
	//std::cout << sizeof(Chin::ParticlePhysicsComponentData) << std::endl; // 64
	//std::cout << sizeof(Chin::PODHandle) << std::endl;
	//std::cout << sizeof(Chin::EntityList) << std::endl; // 36
	//std::cout << sizeof(Chin::Transform) << std::endl; // 48
	//std::cout << sizeof(Chin::LimitedOutput<Chin::Transform, 1>) << std::endl; // 56
	//std::cout << sizeof(Chin::LimitedOutput<Chin::Transform, 2>) << std::endl; // 60
	//std::cout << sizeof(Chin::Entity) << std::endl; // 16
	//std::cout << sizeof(Chin::SpotLight) << std::endl;
	//std::cout << sizeof(Chin::GenericLight) << std::endl;
	std::cout << sizeof(Chin::RequiredInput<Chin::Transform>) << std::endl; // 16
	std::cout << sizeof(std::string) << std::endl; // 28
	std::cout << sizeof(int) << std::endl; // 4

	system("PAUSE");
	return 0;
}

sf::ContextSettings defaultContextSettings()
{
	sf::ContextSettings Settings;
    Settings.depthBits = 32;
    Settings.majorVersion = 4;
    Settings.minorVersion = 5;
    Settings.antialiasingLevel = 4;

	return Settings;
}
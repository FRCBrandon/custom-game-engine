#pragma once
#ifndef DYNAMIC_CONTIGUOUS_ALLOCATOR_H
#define DYNAMIC_CONTIGUOUS_ALLOCATOR_H

#include "Core/core.h"
#include <optional>

namespace Chin
{
	// Dynamic Contiguous Pool Allocator
	// Unsafely typed
	// Not Aligned
	class DynamicContiguousAllocator
	{
	public:
		explicit DynamicContiguousAllocator(size_t sizeOfBlocks, size_t reservedSize = 1024, size_t maxCapacity = 0) : m_sizeOfBlocks(sizeOfBlocks), m_capacity(reservedSize), m_maxCapacity(maxCapacity)
		{
			const size_t size = reservedSize;
			ASSERT(reservedSize > 0);

			m_allocated.reserve(reservedSize);
			m_allocated.assign(size, 0);

			m_arena = malloc(size * sizeOfBlocks);
			m_endOfArena = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(m_arena) + (size * sizeOfBlocks));
			m_endOfAllocatedMemory = m_arena;

			if (maxCapacity == 0)
				m_maxCapacity = (std::numeric_limits<size_t>::max() / m_sizeOfBlocks) + 1;
		}

		// Frees memory
		// Doesn't call destructors
		~DynamicContiguousAllocator()
		{
			free(m_arena);
		}

		void Grow();

		// Returns pointer to memory block
		// Use with placement new
		void* Allocate(size_t sz);

		// Returns last element's new address
		void Deallocate(void* data);

		// Returns pointer to first index
		void* GetArena()
		{
			return m_arena;
		}

		void* GetEndOfArena()
		{
			return m_endOfArena;
		}

		// Unsafe
		// Needs to check
		void* GetBlockAddress(size_t index)
		{
			return reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(m_arena) + (index * m_sizeOfBlocks));
		}

		// Unsafe
		// Needs to check
		// Maybe don't check
		size_t GetBlockIndex(void* address)
		{
			return ((reinterpret_cast<uintptr_t>(address) - reinterpret_cast<uintptr_t>(m_arena)) / m_sizeOfBlocks);
		}

		void* GetBeginOfAllocatedMemory()
		{
			return GetArena();
		}

		void* GetEndOfAllocatedMemory()
		{
			return m_endOfAllocatedMemory;
		}

		// Return amount of elements allocated
		size_t GetAmountOfElementsAllocated()
		{
			return m_elementsAllocated;
		}

		// The size of a single block in bytes
		size_t GetSizeOfBlocks()
		{
			return m_sizeOfBlocks;
		}

		// Return true if Set capacity Has been surpassed
		bool HasGrown()
		{
			return m_grown;
		}

		void ResetGrown()
		{
			m_grown = false;
		}

		// Returns current capacity of allocator
		size_t GetCapacity()
		{
			return m_capacity;
		}

		// Absolute maximum elements allowed
		// Cannot grow past this size
		size_t GetMaxCapacity()
		{
			return m_maxCapacity;
		}

		// Returns true if out of bounds
		bool IsOutOfBounds(void* addressPtr)
		{
			const uintptr_t address = reinterpret_cast<uintptr_t>(addressPtr);
			return address >= reinterpret_cast<uintptr_t>(m_arena) && address <= reinterpret_cast<uintptr_t>(m_endOfArena) ? false : true;
		}

		// Returns true if out of bounds
		bool IsOutOfBounds(uint16_t index)
		{
			return index <= GetCapacity() - 1 ? false : true;
		}

		// Returns true if elements allocated Is equal to capacity
		bool IsAtCapacity()
		{
			return m_elementsAllocated == GetCapacity() ? true : false;
		}

		// Returns true if elements allocated Is equal to max capacity
		bool IsAtMaxCapacity()
		{
			return m_elementsAllocated == GetMaxCapacity() ? true : false;
		}

		// Returns address at index
		void* operator[](size_t index)
		{
			return GetBlockAddress(index);
		}
	private:
		size_t m_elementsAllocated = 0; // Amount of elements allocated
		size_t m_sizeOfBlocks = 0; // Size of blocks
		size_t m_capacity = 0; // Current capacity of memory block
		size_t m_holeCount = 0; // Amount of holes in allocated memory CAN BE REMOVED
		bool m_grown = false; // If the allocated memory Has grown
		void *m_arena = nullptr; // Points to beginning of memory block
		void *m_endOfArena = nullptr; // Points to end of memory block
		void *m_endOfAllocatedMemory = nullptr; // Points to end of allocated memory
		size_t m_maxCapacity = 0;
		std::vector<char> m_allocated; // Keeps track which blocks of data are allocated or not
	public:
		struct MaxElementsException : public Exception
		{
			const char* what() const throw()
			{
				return "Dyanmic Contiguous Allocator max capacity reached";
			}
		};
	};

	template<typename T>
	void AllocatorDelete(T* ptr, DynamicContiguousAllocator& allocator)
	{
		if (ptr)
		{
			ptr->~T();
			allocator.Deallocate(static_cast<void*>(ptr));
		}
	}
}

void* operator new(size_t sz, Chin::DynamicContiguousAllocator& allocator);

#endif
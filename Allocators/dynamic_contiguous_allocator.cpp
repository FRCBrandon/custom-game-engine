#include "dynamic_contiguous_allocator.h"
#include <iostream>

namespace Chin
{
	// Fix Vector
	void DynamicContiguousAllocator::Grow()
	{
		const size_t newCapacity = m_capacity * 2;
		const size_t newSizeOfMemoryInBytes = newCapacity * m_sizeOfBlocks;

		// Might be more efficient?
		// m_arena = realloc(m_arena, newSize * m_sizeOfBlock);

		void *ptr = malloc(newSizeOfMemoryInBytes);
		memcpy(ptr, m_arena, m_capacity * m_sizeOfBlocks);
		free(m_arena);

		m_arena = ptr;
		m_endOfArena = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(ptr) + newSizeOfMemoryInBytes);
		m_endOfAllocatedMemory = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(ptr) + GetAmountOfElementsAllocated() * m_sizeOfBlocks);

		m_allocated.resize(newCapacity, false);

		m_capacity = newCapacity;
		m_grown = true;
	}

	void* DynamicContiguousAllocator::Allocate(size_t sz)
	{
		ASSERT(sz == m_sizeOfBlocks);
		// Division might jank up
		if (IsAtCapacity())
		{
			if (IsAtMaxCapacity())
			{
				throw MaxElementsException();
				return nullptr;
			}
			Grow();
		}

		size_t i = 0;
		for (; m_allocated[i] != false; i++);

		m_allocated[i] = true;
		m_elementsAllocated++;

		auto address = reinterpret_cast<uintptr_t>(m_arena) + (i * m_sizeOfBlocks);

		m_endOfAllocatedMemory = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(m_endOfAllocatedMemory) + m_sizeOfBlocks);

		return reinterpret_cast<void*>(address);
	}

	void DynamicContiguousAllocator::Deallocate(void* data)
	{
		if (IsOutOfBounds(data))
		{
			return;
		}

		//data->~TYPE(); // explicit destructor call
		ptrdiff_t offSet = reinterpret_cast<uintptr_t>(data) - reinterpret_cast<uintptr_t>(m_arena);

		const size_t releasedIndex = (offSet / m_sizeOfBlocks);

		m_elementsAllocated--;

		const size_t lastElementIndex = GetAmountOfElementsAllocated();

		m_allocated[lastElementIndex] = false;

		memcpy(data, GetBlockAddress(lastElementIndex), m_sizeOfBlocks);

		m_endOfAllocatedMemory = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(m_endOfAllocatedMemory) - m_sizeOfBlocks);

		// Return if released Is last element
		if (releasedIndex == lastElementIndex)
		{
			return;
		}

		return;
	}
}

void* operator new(size_t sz, Chin::DynamicContiguousAllocator& allocator)
{
	return allocator.Allocate(sz);
}

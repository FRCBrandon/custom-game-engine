#pragma once
#ifndef UNIT_TEST_H
#define UNIT_TEST_H

#include "Modules/Common/Components/transformcomponent.h"
#include "Modules/Physics/Components/particle_physics_component.h"
#include <iostream>

namespace Chin
{
	namespace UnitTest
	{
		static size_t count = 1;

		void Success()
		{
			std::cout << "Case " << count++ << ": Success" << "\n";
		}

		void ReSet()
		{
			count = 1;
		}

		struct Base
		{
			virtual ~Base() {}
			virtual int Fn() = 0;
		};

		struct Abra : public Base
		{
			int m_a;

			Abra(int a) : m_a(a)
			{
				std::cout << "construct: " << m_a << std::endl;
			}

			~Abra()
			{
				std::cout << "destruct: " << m_a << std::endl;
			}

			int Fn() final
			{
				std::cout << m_a;
				return m_a;
			}
		};

		void DynamicContiguousAllocatorTest();

		void DynamicHandleTest();

		void EntityComponentSystemTest();
	}
}

#endif
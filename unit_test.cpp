#include "unit_test.h"

int main1()
{
	Chin::UnitTest::EntityComponentSystemTest();

	system("PAUSE");

	return 0;
}

namespace Chin
{
	namespace UnitTest
	{
		void DynamicContiguousAllocatorTest()
		{
			ReSet();

			// Case 1: Normal Construction
			DynamicContiguousAllocator* instance = new DynamicContiguousAllocator(sizeof(Abra), 8, 16);

			Success();

			// Case 2: Allocating 1 element
			void* address = instance->Allocate(sizeof(Abra));
			Base* element = new (address) Abra(1);
			element->Fn();
			if (auto ptr = dynamic_cast<Abra*>(element))
			{
				ASSERT(ptr->Fn() == 1);
				std::cout << "\n";
			}

			Success();

			// Case 3: Delete element
			AllocatorDelete(element, *instance);

			Success();

			// Case 4: Allocate 8 elements
			for (size_t i = 0; i < 8; i++)
			{
				void* address = instance->Allocate(sizeof(Abra));
				Abra* element = new (address) Abra(i);
			}

			DynamicContiguousAllocator& instanceRef = *instance;

			Success();

			// Case 5: Use all elements
			for (size_t i = 0; i < 8; i++)
			{
				static_cast<Abra*>(instanceRef[i])->Fn();
			}

			std::cout << "\n";

			Success();

			// Case 6: Delete 5th element, 2nd, last, and first
			AllocatorDelete(instanceRef[4], instanceRef);

			for (size_t i = 0; i < 7; i++)
			{
				static_cast<Abra*>(instanceRef[i])->Fn();
			}

			std::cout << std::endl;

			AllocatorDelete(instanceRef[1], instanceRef);

			for (size_t i = 0; i < 6; i++)
			{
				static_cast<Abra*>(instanceRef[i])->Fn();
			}

			std::cout << std::endl;

			AllocatorDelete(instanceRef[5], instanceRef);

			for (size_t i = 0; i < 5; i++)
			{
				static_cast<Abra*>(instanceRef[i])->Fn();
			}

			std::cout << std::endl;

			AllocatorDelete(instanceRef[0], instanceRef);

			for (size_t i = 0; i < 4; i++)
			{
				static_cast<Abra*>(instanceRef[i])->Fn();
			}

			std::cout << std::endl;

			Success();

			// Case 7: Use all elements
			for (size_t i = 0; i < 4; i++)
			{
				static_cast<Abra*>(instanceRef[i])->Fn();
			}

			std::cout << "\n";

			Success();

			// Case 8: Allocate 5 to invoke Grow
			for (size_t i = 0; i < 5; i++)
			{
				void* address = instance->Allocate(sizeof(Abra));
				Abra* element = new (address) Abra(i);
			}

			Success();

			// Case 9: reSet HasGrown
			ASSERT(instanceRef.HasGrown());
			instanceRef.ResetGrown();
			ASSERT(!instanceRef.HasGrown());

			Success();

			// Case 10: Allocate past max
			try
			{
				for (size_t i = 0; i < 8; i++)
				{
					void* address = instance->Allocate(sizeof(Abra));
					Abra* element = new (address) Abra(i);
				}
			}
			catch (std::exception& e)
			{
				std::cout << "Exception Success: " << e.what() << std::endl;
			}

			Success();

			// Case 11: Normal Destruction
			delete instance;

			Success();

			// Case 12: Bad Construction

			// Case 13: Out of range access

			// Out of range deletion

			// Wrong sized allocation
		}

		void DynamicHandleTest()
		{
			ReSet();

			// Case 1: Normal Construction
			DynamicHandle<Abra>* handle = new DynamicHandle<Abra>(8);

			Success();

			// Calls construct once
			// Case 2: Add element
			auto tempHandle = handle->ConstructElement(0);

			Success();

			// Case 3: Use element
			handle->Get(tempHandle)->Fn();

			std::cout << "\n";

			Success();

			// Calls destruct
			// Case 4: Delete element
			handle->DestructElement(tempHandle);

			Success();

			auto a = handle->Get(tempHandle);

			// Case 5: Get deleted element
			ASSERT(handle->Get(tempHandle) == nullptr);

			Success();

			// Case 6: Add elements
			for (size_t i = 0; i < 8; i++)
			{
				handle->ConstructElement(i);
			}

			Success();

			// Case 7: Use elements
			for (auto it = handle->Begin(); it != handle->End(); it++)
			{
				it->Fn();
			}

			std::cout << "\n";

			Success();

			// Case 8: Attempt to use expired handle to delete
			handle->DestructElement(tempHandle);

			Success();

			// Case 9: Store handle
			auto storedHandle = handle->GetHandle(3);

			Success();

			// Case 10: Delete 5th element, 2nd, last, and first
			handle->DestructElement(4);

			for (auto it = handle->Begin(); it != handle->End(); it++)
			{
				it->Fn();
			}

			std::cout << "\n";

			handle->DestructElement(1);

			for (auto it = handle->Begin(); it != handle->End(); it++)
			{
				it->Fn();
			}

			std::cout << "\n";

			handle->DestructElement(5);

			for (auto it = handle->Begin(); it != handle->End(); it++)
			{
				it->Fn();
			}

			std::cout << "\n";

			handle->DestructElement(0);

			for (auto it = handle->Begin(); it != handle->End(); it++)
			{
				it->Fn();
			}

			std::cout << "\n";

			Success();

			// Case 11: Use stored handle
			handle->Get(storedHandle)->Fn();

			std::cout << "\n";

			Success();

			// Case 12: Invoke grow
			for (size_t i = 0; i < 5; i++)
			{
				handle->ConstructElement(i);
			}

			ASSERT(handle->HasGrown());
			handle->ResetGrown();
			ASSERT(!handle->HasGrown());

			Success();

			// Case 13: Use elements
			for (auto it = handle->Begin(); it != handle->End(); it++)
			{
				it->Fn();
			}

			std::cout << "\n";

			Success();

			// Note: destructs from last to first
			// Case 14: Normal destruction
			delete handle;

			Success();
		}

		void ParticlePhysicsComponentTest()
		{
		}

		void EntityTest()
		{
		}

		void EntityComponentSystemTest()
		{
			ReSet();

			// Case 1: Component Registry construction
			ComponentRegistry& componentRegistry = Entity::GetComponentRegistry();

			Success();

			// Case 2: Normal component construction
			TransformComponent* transformComponent = new TransformComponent(8);
			ParticlePhysicsComponent* physicsComponent = new ParticlePhysicsComponent();

			Success();

			// Case 3: Register components
			componentRegistry.RegisterComponent(transformComponent);
			componentRegistry.RegisterComponent(physicsComponent);

			Success();

			// Case 4: Normal entity construction
			Entity* entity = new Entity(1);

			Success();

			// Case 5: Add component
			entity->AddComponent("Transform");

			Success();

			// Case 6: Add a component with construction and linking
			entity->AddComponent("Particle Physics");

			Success();

			// Case 7: GetComponentData with valid component string
			auto baseData = entity->GetComponentData("Particle Physics");
			auto physicsData = dynamic_cast<ParticlePhysicsComponentData*>(baseData);
			auto transformData = dynamic_cast<TransformComponentData*>(entity->GetComponentData("Transform"));

			ASSERT(physicsData);
			ASSERT(entity == physicsData->GetParent());

			Success();

			// Case 8: GetDataField with valid variable name
			auto dataField = physicsData->GetDataField(Hash_String("inverseMass"));
			auto realDataField = dynamic_cast<DataField<real>*>(dataField.get());
			ASSERT(dataField);
			ASSERT(realDataField);

			Success();

			// Case 9: Test DataField
			auto& realVar = *realDataField->Get();

			std::cout << realVar << std::endl;
			physicsData->SetInverseMass(1.0);
			std::cout << realVar << std::endl;

			Success();

			// Case 10: Make sure no duplicates
			entity->AddComponent("Transform");

			Success();

			// Case 11: Test adding non-existent component
			entity->AddComponent("Nonexistent Component");

			Success();

			// Case 12: Normal use of component
			*static_cast<real*>(physicsData->GetVariablePtr(Hash_String("damping"))) = 1.0;
			static_cast<vector3*>(physicsData->GetVariablePtr(Hash_String("acceleration")))->AddScaledVector(vector3(1.0, 1.0, 1.0), 1.0);
			physicsData->Fn(1);

			auto vel = static_cast<vector3*>(physicsData->GetVariablePtr(Hash_String("velocity")));
			std::cout << vel->GetX() << "/" << vel->GetY() << "/" << vel->GetZ() << std::endl;

			Success();

			// Case 13: Test deleting transform component and HasComponent
			entity->DeleteComponent("Transform");

			ASSERT(!entity->HasComponent("Transform"));

			Success();

			// Case 14: Test using component with invalid input
			try
			{
				physicsData->Fn(1.0);
			}
			catch (Exception& e)
			{
				std::cerr << e.what() << std::endl;
				std::cout << vel->GetX() << "/" << vel->GetY() << "/" << vel->GetZ() << std::endl;
			}

			Success();

			// Case 15: Test Getting non-existent data
			ASSERT(entity->GetComponentData("Not Real") == nullptr );

			Success();

			// Case 16: Readding transform component with CreateLink
			entity->AddComponent("Transform");

			ASSERT(physicsData->CreateLink(Hash_String("position"), "Transform", Hash_String("transform")));

			physicsData->Fn(1);
			std::cout << vel->GetX() << "/" << vel->GetY() << "/" << vel->GetZ() << std::endl;

			Success();

			// Case 17: Bad destruction order
			delete transformComponent;
			try
			{
				physicsData->Fn(1);
			}
			catch(Chin::InputBase::InvalidOutputException &e)
			{
				Console::Exception(e);
				std::cout << "Transform deleted early and thrown exception Has been caught" << std::endl;
			}

			delete physicsComponent;

			Success();

			// Case 18: Destruction of entity after components
			delete entity;

			Success();
		}
	}
}
#ifndef VERSION_H
#define VERSION_H

namespace AutoVersion{
	
	//Date Version Types
	static const char DATE[] = "23";
	static const char MONTH[] = "08";
	static const char YEAR[] = "2017";
	static const char UBUNTU_VERSION_STYLE[] =  "17.08";
	
	//Software Status
	static const char STATUS[] =  "Alpha";
	static const char STATUS_SHORT[] =  "a";
	
	//Standard Version Type
	static const long MAJOR  = 3;
	static const long MINOR  = 6;
	static const long BUILD  = 2840;
	static const long REVisION  = 15691;
	
	//Miscellaneous Version Types
	static const long BUILDS_COUNT  = 4161;
	#define RC_FILEVERSION 3,6,2840,15691
	#define RC_FILEVERSION_STRING "3, 6, 2840, 15691\0"
	static const char FULLVERSION_STRING [] = "3.6.2840.15691";
	
	//These values are to keep track of your versioning state, don't modify them.
	static const long BUILD_HisTORY  = 40;
	

}
#endif //VERSION_H
